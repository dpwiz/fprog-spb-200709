module Run (run) where

import Import

import Data.Acquire (withAcquire)
import System.Mem (performGC)
import System.CPUTime (getCPUTime)

import qualified SDL
import qualified Vulkan.Core10 as Vk

import Game.Draw.Models (makeScene)
import Game.Draw.Solids (makeSolids)
import Game.Draw.UI (makeUI)
import Game.Draw.Slides (makeSlides)
import Render.Draw (drawFrame)

import qualified Game.Events as Events
import qualified Game.Resources as Resources

run :: RIO AppSetup ()
run = do
  window <- view appSdlWindow

  devName <- views (appVulkanDevice . vdPhysical . pdProperties) Vk.deviceName
  logInfo $ "Running on " <> displayShow devName

  SDL.showWindow window

  vd <- view appVulkanDevice
  withAcquire (Resources.setup vd) contextLoop

contextLoop :: GameState -> RIO AppSetup ()
contextLoop previousGameState = do
  window <- view appSdlWindow
  dev <- view appVulkanDevice
  nextGameState <- withAcquire (Resources.acquire window dev previousGameState) \ctx -> do
    App{..} <- view id
    stateRef <- newSomeRef previousGameState
    let
      appGameLoop = App
        { _appContext = ctx
        , _appState   = stateRef
        , ..
        }
    initial <- liftIO getCPUTime
    runRIO appGameLoop (mainLoop initial initial)

  case nextGameState of
    Just restart ->
      contextLoop restart
    Nothing ->
      logInfo "Bye!"

mainLoop :: Integer -> Integer -> RIO AppGame (Maybe GameState)
mainLoop prevTime startTime = do
  SDL.pollEvents >>= mapM_ Events.handle
  Events.handleTime prevTime startTime

  quitApp <- use gsQuit
  updateContext <- use gsUpdateContext
  if quitApp || updateContext then
    -- XXX: dump final state for new context iteration to pick up
    if quitApp then do
      views appVulkanDevice _vdLogical >>= Vk.deviceWaitIdle
      pure Nothing
    else
      uses id Just
  else do
    World{..} <- use gsWorld
    scenes <- sequence $ concat
      [ [ makeScene  | _worldShowModels  ]
      , [ makeSolids | _worldShowSolids  ]
      , [ makeUI     | _worldShowSprites ]
      , [ makeSlides | _worldShowSlides  ]
      ]
    drawFrame scenes

    Options{optionsFrameGC, optionsFrameDelay} <- view appOptions
    when optionsFrameGC $
      liftIO performGC
    when optionsFrameDelay do
      endTime <- liftIO getCPUTime
      let frameTime = fromIntegral $ (endTime - startTime) `div` 1_000_000 -- XXX: cut to microsecs of threadDelay
      let budget = truncate @Double (1_000_000 / 60) - frameTime
      let delay = max 0 $ budget - 900 -- XXX: extra space for threadDelay non-determinism
      threadDelay delay
      -- logInfo $ "delay: " <> displayShow (frameTime, budget, delay)

    nextTicks <- liftIO getCPUTime
    mainLoop startTime nextTicks
