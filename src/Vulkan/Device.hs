module Vulkan.Device where

import Import

import Control.Monad.Trans.Maybe (MaybeT(..), runMaybeT)
import Data.Acquire (Acquire, mkAcquire)
import Data.List (nub)
import Vulkan.CStruct.Extends (pattern (:&), pattern (::&))
import Vulkan.Version (pattern MAKE_VERSION)

import qualified Data.ByteString as BS
import qualified RIO.Vector as Vector
import qualified SDL
import qualified SDL.Video.Vulkan as SDL (vkGetInstanceExtensions, vkCreateSurface)
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Core11 as Vk11
import qualified Vulkan.Extensions.VK_KHR_surface as Khr
import qualified Vulkan.Extensions.VK_KHR_swapchain as Khr
import qualified VulkanMemoryAllocator as VMA
import qualified Vulkan.Extensions.VK_EXT_debug_utils as Ext
import qualified Vulkan.Extensions.VK_EXT_validation_features as Ext
import qualified Vulkan.Utils.Debug as Utils

import qualified Vulkan.Sampler as Sampler
import qualified Vulkan.SyncObjects as SyncObjects

data DeviceError = DeviceError Text
  deriving (Eq, Ord, Show)

instance Exception DeviceError

withDevice :: Options -> SDL.Window -> Acquire VulkanDevice
withDevice Options{..} sdlWindow = do
  extensions <- liftIO do
    cstrings <- SDL.vkGetInstanceExtensions sdlWindow
    traverse BS.packCString cstrings

  -- TODO: debugExt setup
  let
    layers = map encodeUtf8 $ concat
      [ [ "VK_LAYER_KHRONOS_validation" | optionsValidation ]
      , [ "VK_LAYER_RENDERDOC_Capture"  | optionsRenderdoc  ]
      ]

    validationFeatures =
      [ Ext.VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT
      ]

    instanceCI = zero
      { Vk.enabledLayerNames =
          Vector.fromList layers
      , Vk.enabledExtensionNames =
          Vector.fromList
            $ Ext.EXT_DEBUG_UTILS_EXTENSION_NAME
            : extensions
      , Vk.applicationInfo = Just zero
          { Vk.applicationName = Just "playground 3"
          , Vk.apiVersion      = MAKE_VERSION 1 1 0
          }
      }
        ::& debugMessengerCreateInfo
        :& Ext.ValidationFeaturesEXT (Vector.fromList validationFeatures) mempty
        :& ()
  vkInstance <- Vk.withInstance instanceCI Nothing mkAcquire
  _dm <- Ext.withDebugUtilsMessengerEXT vkInstance debugMessengerCreateInfo Nothing mkAcquire

  surface <- withSdlKhrSurface sdlWindow vkInstance
  physical <- liftIO $ getPhysicalDevice vkInstance surface
  withVulkanDevice vkInstance surface physical

withSdlKhrSurface :: SDL.Window -> Vk.Instance -> Acquire Khr.SurfaceKHR
withSdlKhrSurface sdlWindow vkInstance = mkAcquire create destroy
  where
    create =
      fmap Khr.SurfaceKHR $
        SDL.vkCreateSurface sdlWindow $
          castPtr (Vk.instanceHandle vkInstance)

    destroy o =
      Khr.destroySurfaceKHR vkInstance o Nothing

getPhysicalDevice :: (MonadIO m, MonadThrow m) => Vk.Instance -> Khr.SurfaceKHR -> m PhysicalDevice
getPhysicalDevice vkInstance surface = do
  (_result, devs) <- Vk.enumeratePhysicalDevices vkInstance
  scoreDevices (toList devs) >>= \case
    [] ->
      throwM $ DeviceError "No matching devices."
    (_score, best) : _rest ->
      pure best
  where
    scoreDevices devs =
      fmap
        (sortOn fst . catMaybes)
        (mapMaybeM scoreDevice devs)

    headM x = MaybeT case x of
      [] ->
        pure Nothing
      h : _ead ->
        pure (Just h)

    scoreDevice dev = runMaybeT do
      deviceHasSwapChain dev

      graphicsQueue <- headM =<< getGraphicsQueueIndices dev
      presentQueue  <- headM =<< getPresentQueueIndices dev
      presentMode   <- headM =<< getPresentMode dev
      bestFormat    <- getFormat dev

      props <- Vk.getPhysicalDeviceProperties dev
      feats <- Vk.getPhysicalDeviceFeatures dev
      caps  <- Khr.getPhysicalDeviceSurfaceCapabilitiesKHR dev surface
      mem   <- Vk.getPhysicalDeviceMemoryProperties dev

      pure $ Just
        ( deviceScore dev mem caps
        , PhysicalDevice
            { _pdPhysicalDevice    = dev
            , _pdProperties        = props
            , _pdFeatures          = feats
            , _pdMsaaSamples       = msaaSamples props
            , _pdSamplerAnisotropy = Vk.maxSamplerAnisotropy (Vk.limits props)
            , _pdGraphicsQueueIx   = graphicsQueue
            , _pdPresentQueueIx    = presentQueue
            , _pdPresentMode       = presentMode
            , _pdSurfaceFormat     = bestFormat
            , _pdSurfaceCaps       = caps
            , _pdMemory            = mem
            }
        )

    deviceHasSwapChain dev = do
      (_res, extensions) <- Vk.enumerateDeviceExtensionProperties dev Nothing
      guard $
        Vector.elem
          Khr.KHR_SWAPCHAIN_EXTENSION_NAME
          (fmap Vk.extensionName extensions)

    getGraphicsQueueIndices dev = do
      queueFamilyProperties <- Vk.getPhysicalDeviceQueueFamilyProperties dev
      pure do
        (ix, q) <- zip [0..] $ toList queueFamilyProperties
        guard $
          (Vk.QUEUE_GRAPHICS_BIT .&&. Vk.queueFlags q) &&
          (Vk.queueCount q > 0)
        pure ix

    getPresentQueueIndices dev = fmap concat do
      queues <- Vk11.getPhysicalDeviceQueueFamilyProperties2 @'[] dev
      for (zip [0..] $ toList queues) \(ix, _q) -> do
        support <- Khr.getPhysicalDeviceSurfaceSupportKHR dev ix surface
        pure [ix | support]

    getFormat dev = do
      (_res, formats) <- Khr.getPhysicalDeviceSurfaceFormatsKHR dev surface
      pure case toList formats of
        [] ->
          desiredFormat
        [Khr.SurfaceFormatKHR Vk.FORMAT_UNDEFINED _colorSpace] ->
          desiredFormat
        candidates | any cond candidates ->
          desiredFormat
        whatever : _rest ->
          whatever
      where
        -- XXX: An Eq-substitute? Should be fixed in vulkan-3.4
        cond f =
          Khr.format     (f :: Khr.SurfaceFormatKHR) == Khr.format     (desiredFormat :: Khr.SurfaceFormatKHR) &&
          Khr.colorSpace (f :: Khr.SurfaceFormatKHR) == Khr.colorSpace (desiredFormat :: Khr.SurfaceFormatKHR)

    getPresentMode dev = do
      (_res, presentModes) <- Khr.getPhysicalDeviceSurfacePresentModesKHR dev surface
      pure do
        m <- desiredPresentModes
        guard $ Vector.elem m presentModes
        pure m

    desiredFormat =
      Khr.SurfaceFormatKHR
        Vk.FORMAT_B8G8R8_SRGB
        Khr.COLOR_SPACE_SRGB_NONLINEAR_KHR

    desiredPresentModes =
      [ Khr.PRESENT_MODE_FIFO_KHR
      , Khr.PRESENT_MODE_MAILBOX_KHR
      , Khr.PRESENT_MODE_IMMEDIATE_KHR
      ]

deviceScore
  :: Vk.PhysicalDevice
  -> Vk.PhysicalDeviceMemoryProperties
  -> Khr.SurfaceCapabilitiesKHR
  -> Int
deviceScore _dev mem _caps = fromIntegral totalHeapSize
  where
    totalHeapSize = sum $ do
      Vk.MemoryHeap{size} <- Vk.memoryHeaps mem
      pure size

withVulkanDevice :: Vk11.Instance -> Khr.SurfaceKHR -> PhysicalDevice -> Acquire VulkanDevice
withVulkanDevice vkInstance surface pd@PhysicalDevice{..} = do
  device <- Vk.withDevice _pdPhysicalDevice deviceCI Nothing mkAcquire

  depthFormat <- liftIO $ getOptimalDepthFormat _pdPhysicalDevice

  graphicsQueue <- Vk11.getDeviceQueue2 device zero
    { Vk11.queueFamilyIndex = _pdGraphicsQueueIx
    , Vk11.flags            = Vk11.DEVICE_QUEUE_CREATE_PROTECTED_BIT
    }

  presentQueue <- Vk11.getDeviceQueue2 device zero
    { Vk11.queueFamilyIndex = _pdPresentQueueIx
    , Vk11.flags            = Vk11.DEVICE_QUEUE_CREATE_PROTECTED_BIT
    }

  allocator <- VMA.withAllocator (allocatorCI device) mkAcquire

  commandPool <- Vk.withCommandPool device commandPoolCI Nothing mkAcquire

  syncObjects <- SyncObjects.acquire device

  samplers <- Sampler.acquireAll device _pdSamplerAnisotropy

  pure VulkanDevice
    { _vdLogical        = device
    , _vdPhysical       = pd
    , _vdSurface        = surface
    , _vdSurfaceFormat  = Khr.format _pdSurfaceFormat
    , _vdDepthFormat    = depthFormat
    , _vdGraphicsQ      = graphicsQueue
    , _vdPresentQ       = presentQueue
    , _vdAllocator      = allocator
    , _vdCommandPool    = commandPool
    , _vdSyncObjects    = syncObjects
    , _vdSamplers       = samplers
    }
  where
    requiredDeviceExtensions =
      [ Khr.KHR_SWAPCHAIN_EXTENSION_NAME
      ]

    deviceCI :: Vk.DeviceCreateInfo '[]
    deviceCI = zero
      { Vk.queueCreateInfos = Vector.fromList do
          i <- nub [_pdGraphicsQueueIx, _pdPresentQueueIx]
          pure $ SomeStruct zero
            { Vk.queueFamilyIndex = i
            , Vk.queuePriorities = Vector.singleton 1
            , Vk.flags = Vk.DEVICE_QUEUE_CREATE_PROTECTED_BIT
            }
      , Vk.enabledExtensionNames =
          Vector.fromList requiredDeviceExtensions
      , Vk.enabledFeatures = Just zero
          { Vk.sampleRateShading = _pdMsaaSamples /= Vk.SAMPLE_COUNT_1_BIT
          , Vk.samplerAnisotropy = _pdSamplerAnisotropy > 1.0
          }
      }

    allocatorCI device = zero
      { VMA.physicalDevice  = Vk.physicalDeviceHandle _pdPhysicalDevice
      , VMA.device          = Vk.deviceHandle device
      , VMA.instance'       = Vk.instanceHandle vkInstance
      , VMA.frameInUseCount = 1
      }

    commandPoolCI = Vk.CommandPoolCreateInfo
      { flags            = Vk.COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT
      , queueFamilyIndex = _pdGraphicsQueueIx
      }

getOptimalDepthFormat :: (MonadIO m, MonadThrow m) => Vk.PhysicalDevice -> m Vk.Format
getOptimalDepthFormat pd = do
  properties <- traverse (Vk.getPhysicalDeviceFormatProperties pd) depthCandidates
  let
    matching = do
      (format, props) <- zip depthCandidates properties
      guard $
        Vk.optimalTilingFeatures props .&&. Vk.FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
      pure format
  case matching of
    [] ->
      throwM $ DeviceError "No matching formats"
    best : _rest ->
      pure best

depthCandidates :: [Vk.Format]
depthCandidates =
  [ Vk.FORMAT_D32_SFLOAT_S8_UINT
  , Vk.FORMAT_D24_UNORM_S8_UINT
  , Vk.FORMAT_D32_SFLOAT -- XXX: prefer stencil-available formats
  ]

msaaSamples :: Vk.PhysicalDeviceProperties -> Vk.SampleCountFlagBits
msaaSamples Vk.PhysicalDeviceProperties{limits} =
  case samplesAvailable of
    [] ->
      Vk.SAMPLE_COUNT_1_BIT
    best : _rest ->
      best
  where
    counts =
      Vk.framebufferColorSampleCounts limits .&.
      Vk.framebufferDepthSampleCounts limits

    samplesAvailable = do
      countBit <- msaaCandidates
      guard $ counts .&&. countBit
      pure countBit

msaaCandidates :: [Vk.SampleCountFlagBits]
msaaCandidates =
  [ Vk.SAMPLE_COUNT_16_BIT
  , Vk.SAMPLE_COUNT_8_BIT
  , Vk.SAMPLE_COUNT_4_BIT
  , Vk.SAMPLE_COUNT_2_BIT
  ]

debugMessengerCreateInfo :: Ext.DebugUtilsMessengerCreateInfoEXT
debugMessengerCreateInfo = zero
  { Ext.messageSeverity =
      -- Ext.DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT .|.
      Ext.DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT .|.
      Ext.DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
  , Ext.messageType =
      Ext.DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT .|.
      Ext.DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT .|.
      Ext.DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
  , Ext.pfnUserCallback =
      Utils.debugCallbackFatalPtr
  }
