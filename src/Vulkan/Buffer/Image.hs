{-# LANGUAGE OverloadedLists #-}

module Vulkan.Buffer.Image where

import Import

import Data.Acquire (Acquire, mkAcquire)

import qualified Foreign
import qualified Vulkan.Core10 as Vk
import qualified VulkanMemoryAllocator as VMA

import Vulkan.Buffer.Command (oneshot)

import qualified Resource.Image as Image

pattern IMAGE_FORMAT :: Vk.Format
pattern IMAGE_FORMAT = Vk.FORMAT_R8G8B8A8_SRGB

-- * Image view

acquireImageView
  :: VulkanDevice
  -> Vk.Image
  -> "mipLevels" ::: Word32
  -> Acquire Vk.ImageView
acquireImageView VulkanDevice{..} image mipLevels =
  Vk.withImageView _vdLogical imageViewCI Nothing mkAcquire
  where
    imageViewCI = zero
      { Vk.image            = image
      , Vk.viewType         = Vk.IMAGE_VIEW_TYPE_2D
      , Vk.format           = IMAGE_FORMAT
      , Vk.components       = zero
      , Vk.subresourceRange = subr
      }

    subr = Vk.ImageSubresourceRange
      { aspectMask  = Vk.IMAGE_ASPECT_COLOR_BIT
      , baseMipLevel   = 0
      , levelCount     = mipLevels -- XXX: including base
      , baseArrayLayer = 0
      , layerCount     = 1
      }

-- * Image buffer

acquireImage :: VulkanDevice -> FilePath -> Acquire (Vk.Image, "mipLevels" ::: Word32)
acquireImage vd path = do
  (image, mipLevels, _alloc) <- mkAcquire (createImage vd path) (destroyImage vd)
  pure (image, mipLevels)

createImage
  :: (MonadUnliftIO m) -- , MonadReader env m, HasLogFunc env)
  => VulkanDevice
  -> FilePath
  -> m (Vk.Image, "mipLevels" ::: Word32, VMA.Allocation)
createImage vd@VulkanDevice{..} path =
  Image.loadWith path $ \width height pixelsPtr -> do
    let pixelBytes = fromIntegral $ width * height * 4
    let extent = Vk.Extent3D width height 1
    let mipLevels = log2MipLevels extent

    -- logDebug $ displayShow ((width, height), (pixelsPtr, pixelBytes))

    (image, allocation, _info) <- VMA.createImage _vdAllocator (imageCI extent) imageAllocationCI

    VMA.withBuffer _vdAllocator (stageBufferCI pixelBytes) stageAllocationCI bracket $ \(staging, stage, _stageInfo) -> do
      VMA.withMappedMemory _vdAllocator stage bracket $ \stagePtr ->
        liftIO $ Foreign.copyBytes stagePtr pixelsPtr pixelBytes

      -- logDebug "Image transition started"

      transitionImageLayout
        vd
        image
        mipLevels
        IMAGE_FORMAT
        Vk.IMAGE_LAYOUT_UNDEFINED
        Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL

      -- logDebug "Image transfer started"

      copyBufferToImage vd staging image extent

      -- logDebug "Image transfer ended"

      -- XXX: converting Word32 sizes to Int32 offsets, huh?
      generateMipmaps vd image (fromIntegral width) (fromIntegral height) mipLevels

      -- logDebug "Image mipmaps generated"

    pure (image, mipLevels, allocation)
  where
    usage =
      Vk.IMAGE_USAGE_SAMPLED_BIT .|.      -- Sampler
      Vk.IMAGE_USAGE_TRANSFER_DST_BIT .|. -- Staging
      Vk.IMAGE_USAGE_TRANSFER_SRC_BIT     -- Mip generation

    imageCI :: Vk.Extent3D -> Vk.ImageCreateInfo '[]
    imageCI extent = zero
      { Vk.imageType     = Vk.IMAGE_TYPE_2D
      , Vk.format        = IMAGE_FORMAT
      , Vk.extent        = extent
      , Vk.mipLevels     = log2MipLevels extent
      , Vk.arrayLayers   = 1
      , Vk.tiling        = Vk.IMAGE_TILING_OPTIMAL
      , Vk.initialLayout = Vk.IMAGE_LAYOUT_UNDEFINED
      , Vk.usage         = usage
      , Vk.sharingMode   = Vk.SHARING_MODE_EXCLUSIVE
      , Vk.samples       = Vk.SAMPLE_COUNT_1_BIT -- XXX: no multisampling here
      }

    imageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_GPU_ONLY
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

    stageBufferCI :: Int -> Vk.BufferCreateInfo '[]
    stageBufferCI pixelBytes = zero
      { Vk.size        = fromIntegral pixelBytes
      , Vk.usage       = Vk.BUFFER_USAGE_TRANSFER_SRC_BIT
      , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
      }

    stageAllocationCI :: VMA.AllocationCreateInfo
    stageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_HOST_VISIBLE_BIT
      }

destroyImage :: VulkanDevice -> (Vk.Image, "mipLevels" ::: Word32, VMA.Allocation) -> IO ()
destroyImage vw (image, _mipLevels, allocation) = VMA.destroyImage (_vdAllocator vw) image allocation

transitionImageLayout
  :: (MonadUnliftIO m)
  => VulkanDevice
  -> Vk.Image
  -> "mipLevels" ::: Word32
  -> Vk.Format
  -> ("old" ::: Vk.ImageLayout)
  -> ("new" ::: Vk.ImageLayout)
  -> m ()
transitionImageLayout vw image mipLevels format old new =
  oneshot vw $ \buf ->
    case (old, new) of
      (Vk.IMAGE_LAYOUT_UNDEFINED, Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) ->
        Vk.cmdPipelineBarrier
          buf
          Vk.PIPELINE_STAGE_TOP_OF_PIPE_BIT
          Vk.PIPELINE_STAGE_TRANSFER_BIT
          zero
          mempty
          mempty
          [ barrier Vk.IMAGE_ASPECT_COLOR_BIT zero Vk.ACCESS_TRANSFER_WRITE_BIT
          ]
      (Vk.IMAGE_LAYOUT_UNDEFINED, Vk.IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) ->
        Vk.cmdPipelineBarrier
          buf
          Vk.PIPELINE_STAGE_TOP_OF_PIPE_BIT
          Vk.PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT
          zero
          mempty
          mempty
          [ barrier aspectMask zero $
              Vk.ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT .|.
              Vk.ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT
          ]
        where
          aspectMask =
            if hasStencilComponent then
              Vk.IMAGE_ASPECT_DEPTH_BIT .|. Vk.IMAGE_ASPECT_STENCIL_BIT
            else
              Vk.IMAGE_ASPECT_DEPTH_BIT
          hasStencilComponent =
            format == Vk.FORMAT_D32_SFLOAT_S8_UINT ||
            format == Vk.FORMAT_D24_UNORM_S8_UINT

      (Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) ->
        Vk.cmdPipelineBarrier
          buf
          Vk.PIPELINE_STAGE_TRANSFER_BIT
          Vk.PIPELINE_STAGE_FRAGMENT_SHADER_BIT
          zero
          mempty
          mempty
          [ barrier Vk.IMAGE_ASPECT_COLOR_BIT Vk.ACCESS_TRANSFER_WRITE_BIT Vk.ACCESS_SHADER_READ_BIT
          ]
      _ ->
        error $ "Unsupported image layout transfer: " <> show (old, new)
  where
    barrier aspectMask srcMask dstMask = SomeStruct zero
      { Vk.srcAccessMask       = srcMask
      , Vk.dstAccessMask       = dstMask
      , Vk.oldLayout           = old
      , Vk.newLayout           = new
      , Vk.srcQueueFamilyIndex = Vk.QUEUE_FAMILY_IGNORED
      , Vk.dstQueueFamilyIndex = Vk.QUEUE_FAMILY_IGNORED
      , Vk.image               = image
      , Vk.subresourceRange    = subresource aspectMask mipLevels
      }

copyBufferToImage
  :: (MonadUnliftIO m)
  => VulkanDevice
  -> Vk.Buffer
  -> Vk.Image
  -> Vk.Extent3D
  -> m ()
copyBufferToImage vw src dst extent =
  oneshot vw $ \buf ->
    Vk.cmdCopyBufferToImage buf src dst Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL [region]
  where
    region = zero
      { Vk.imageSubresource = Vk.ImageSubresourceLayers
          { aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
          , mipLevel       = 0
          , baseArrayLayer = 0
          , layerCount     = 1
          }
      , Vk.imageExtent = extent
      }

generateMipmaps
  :: (MonadUnliftIO m) -- , MonadReader env m, HasLogFunc env)
  => VulkanDevice
  -> Vk.Image
  -> Foreign.Int32
  -> Foreign.Int32
  -> Word32
  -> m ()
generateMipmaps _vw _image _width _height 0 =
  pure ()
  -- logDebug $ "Skipping mipmaps " <> displayShow image
generateMipmaps vw image width height mipLevels = do
  -- logDebug $ "Generating mipmaps " <> displayShow (image, mipRange)
  oneshot vw $ \buf -> do
    for_ mipRange $ \baseLevel -> do
      let mipLevel = baseLevel + 1
      let _dims = (mipWidth mipLevel, mipHeight mipLevel)
      let baseSubr = subr baseLevel

      -- logDebug $ "Transferring mip base: " <> displayShow baseLevel
      cmdBarrier buf Vk.PIPELINE_STAGE_TRANSFER_BIT barrierBase
        { Vk.subresourceRange = baseSubr
        , Vk.oldLayout        = Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        , Vk.newLayout        = Vk.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        , Vk.srcAccessMask    = Vk.ACCESS_TRANSFER_WRITE_BIT
        , Vk.dstAccessMask    = Vk.ACCESS_TRANSFER_READ_BIT
        }

      -- logDebug $ "Blitting " <> displayShow (baseLevel, mipLevel, dims)
      Vk.cmdBlitImage
        buf
        image Vk.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        image Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        [blit $ baseLevel + 1]
        Vk.FILTER_LINEAR

      -- logDebug $ "Locking mip base: " <> displayShow baseLevel
      cmdBarrier buf Vk.PIPELINE_STAGE_FRAGMENT_SHADER_BIT barrierBase
        { Vk.subresourceRange = baseSubr
        , Vk.oldLayout        = Vk.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        , Vk.newLayout        = Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        , Vk.srcAccessMask    = Vk.ACCESS_TRANSFER_READ_BIT
        , Vk.dstAccessMask    = Vk.ACCESS_SHADER_READ_BIT
        }

    -- logDebug $ "Locking last mip: " <> displayShow lastLevel
    cmdBarrier buf Vk.PIPELINE_STAGE_FRAGMENT_SHADER_BIT barrierBase
      { Vk.subresourceRange = subr lastLevel
      , Vk.oldLayout        = Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
      , Vk.newLayout        = Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
      , Vk.srcAccessMask    = Vk.ACCESS_TRANSFER_WRITE_BIT
      , Vk.dstAccessMask    = Vk.ACCESS_SHADER_READ_BIT
      }
  where
    lastLevel = mipLevels - 1
    mipRange = [0 .. lastLevel - 1] :: [Word32]

    cmdBarrier buf dstStage barrier = Vk.cmdPipelineBarrier
      buf
      Vk.PIPELINE_STAGE_TRANSFER_BIT
      dstStage
      zero
      mempty
      mempty
      [SomeStruct barrier]

    barrierBase = zero
      { Vk.srcQueueFamilyIndex = Vk.QUEUE_FAMILY_IGNORED
      , Vk.dstQueueFamilyIndex = Vk.QUEUE_FAMILY_IGNORED
      , Vk.image               = image
      }

    subr baseMip = Vk.ImageSubresourceRange
      { aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
      , baseMipLevel   = baseMip
      , baseArrayLayer = 0
      , levelCount     = 1
      , layerCount     = 1
      }

    blit level = Vk.ImageBlit
      { srcSubresource = srcSubr
      , srcOffsets     = (zero, srcOffset)
      , dstSubresource = dstSubr
      , dstOffsets     = (zero, dstOffset)
      }
      where
        predLevel = level - 1

        srcSubr = Vk.ImageSubresourceLayers
          { aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
          , mipLevel       = predLevel
          , baseArrayLayer = 0
          , layerCount     = 1
          }
        dstSubr = Vk.ImageSubresourceLayers
          { aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
          , mipLevel       = level
          , baseArrayLayer = 0
          , layerCount     = 1
          }
        srcOffset = Vk.Offset3D (mipWidth predLevel) (mipHeight predLevel) 1
        dstOffset = Vk.Offset3D (mipWidth level) (mipHeight level) 1

    mipScale level x = max 1 . truncate @Double $ (0.5 ^ level) * fromIntegral x
    mipWidth  level = mipScale level width
    mipHeight level = mipScale level height

subresource
  :: Vk.ImageAspectFlags
  -> "mipLevels" ::: Word32
  -> Vk.ImageSubresourceRange
subresource aspectMask mipLevels = Vk.ImageSubresourceRange
  { aspectMask     = aspectMask
  , baseMipLevel   = 0
  , levelCount     = mipLevels -- XXX: including base
  , baseArrayLayer = 0
  , layerCount     = 1
  }

class MipLevels a where
  -- | Base level + a number of halvings before it turns into a single pixel.
  log2MipLevels :: Integral b => a -> b

instance Integral a => MipLevels (a, a) where
  log2MipLevels (w, h) = 1 + truncate @Double (logBase 2 . fromIntegral $ max w h)
  {-# INLINE log2MipLevels #-}

instance Integral a => MipLevels (V2 a) where
  log2MipLevels (V2 w h) = log2MipLevels (w, h)

instance MipLevels Vk.Extent2D where
  log2MipLevels (Vk.Extent2D w h) = log2MipLevels (w, h)

instance MipLevels Vk.Extent3D where
  log2MipLevels (Vk.Extent3D w h _d) = log2MipLevels (w, h)
