module Vulkan.Buffer.Uniform where

import Import

import Data.Acquire (Acquire, mkAcquire)
import Data.Maybe (fromJust)

import qualified Foreign
import qualified Vulkan.Core10 as Vk
import qualified VulkanMemoryAllocator as VMA


acquire
  :: (HasCallStack, Storable a)
  => VulkanDevice -> Maybe a -> Acquire (Allocated Vk.Buffer a)
acquire vd@VulkanDevice{..} initial = do
  (buf, allocation, info) <- VMA.withBuffer _vdAllocator uniformCI uniformAI mkAcquire

  let
    allocated = Allocated
      { _allocated      = buf
      , _allocation     = allocation
      , _allocationInfo = info
      }

  case initial of
    Nothing ->
      pure ()
    Just value ->
      liftIO $ update vd allocated value

  pure allocated

  where
    uniformCI = zero
      { Vk.size        = fromIntegral $ Foreign.sizeOf (fromJust initial)
      , Vk.usage       = Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT
      , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
      }

    uniformAI = zero
      { VMA.usage = VMA.MEMORY_USAGE_GPU_ONLY
      , VMA.requiredFlags =
          Vk.MEMORY_PROPERTY_HOST_VISIBLE_BIT .|.
          Vk.MEMORY_PROPERTY_HOST_COHERENT_BIT
      }

-- TODO: wrap mapped memory in Acquire
update :: (Storable a, MonadUnliftIO m) => VulkanDevice -> Allocated buf a -> a -> m ()
update VulkanDevice{..} Allocated{..} value =
  VMA.withMappedMemory _vdAllocator _allocation bracket \ptr ->
    liftIO $ Foreign.poke (Foreign.castPtr ptr) value
