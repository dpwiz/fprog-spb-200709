module Vulkan.Sampler where

import Import

import Data.Acquire (Acquire, mkAcquire)
import qualified RIO.Vector as Vector

import qualified Vulkan.Core10 as Vk

acquireAll :: Vk.Device -> "maxAnisotropy" ::: Float -> Acquire (Vector Vk.Sampler)
acquireAll logical maxAnisotropy = sequence do
  (filt, mips, reps) <- Vector.fromList
    [ (Vk.FILTER_LINEAR,  0,                 Vk.SAMPLER_ADDRESS_MODE_REPEAT)          -- 0
    , (Vk.FILTER_LINEAR,  0,                 Vk.SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER) -- 1
    , (Vk.FILTER_LINEAR,  Vk.LOD_CLAMP_NONE, Vk.SAMPLER_ADDRESS_MODE_REPEAT)          -- 2
    , (Vk.FILTER_LINEAR,  Vk.LOD_CLAMP_NONE, Vk.SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER) -- 3
    , (Vk.FILTER_NEAREST, 0,                 Vk.SAMPLER_ADDRESS_MODE_REPEAT)          -- 4
    , (Vk.FILTER_NEAREST, 0,                 Vk.SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER) -- 5
    , (Vk.FILTER_NEAREST, Vk.LOD_CLAMP_NONE, Vk.SAMPLER_ADDRESS_MODE_REPEAT)          -- 6
    , (Vk.FILTER_NEAREST, Vk.LOD_CLAMP_NONE, Vk.SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER) -- 7
    ]
  pure $ acquire logical maxAnisotropy filt mips reps

acquire :: Vk.Device -> Float -> Vk.Filter -> Float -> Vk.SamplerAddressMode -> Acquire Vk.Sampler
acquire logical maxAnisotropy filt mips reps = Vk.withSampler logical samplerCI Nothing mkAcquire
  where
    samplerCI = zero
      { Vk.magFilter               = filt
      , Vk.minFilter               = filt
      , Vk.addressModeU            = reps
      , Vk.addressModeV            = reps
      , Vk.addressModeW            = reps
      , Vk.anisotropyEnable        = maxAnisotropy > 1
      , Vk.maxAnisotropy           = maxAnisotropy
      , Vk.borderColor             = Vk.BORDER_COLOR_INT_OPAQUE_BLACK
      , Vk.unnormalizedCoordinates = False
      , Vk.compareEnable           = False
      , Vk.compareOp               = Vk.COMPARE_OP_ALWAYS
      , Vk.mipmapMode              = Vk.SAMPLER_MIPMAP_MODE_LINEAR
      , Vk.mipLodBias              = 0
      , Vk.minLod                  = 0
      , Vk.maxLod                  = mips
      }
