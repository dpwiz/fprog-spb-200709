module Vulkan.SyncObjects where

import Import

import Data.Acquire (Acquire, mkAcquire)

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk

-- | Intel says:
--
--     Having two sets of frame resources is a must.
--     Increasing the number of frame resources to three
--     doesn't give us much more performance (if any at all).
--
-- https://software.intel.com/en-us/articles/practical-approach-to-vulkan-part-1
pattern INFLIGHT_FRAMES :: Int
pattern INFLIGHT_FRAMES = 2

acquire :: Vk.Device -> Acquire (Vector SyncObject)
acquire vd = Vector.replicateM INFLIGHT_FRAMES (acquire1 vd)

acquire1 :: Vk.Device -> Acquire SyncObject
acquire1 device = do
  SyncObject
  <$> Vk.withSemaphore device zero Nothing mkAcquire
  <*> Vk.withSemaphore device zero Nothing mkAcquire
  <*> Vk.withFence device fenceCI Nothing mkAcquire
  where
    fenceCI = zero
      { Vk.flags = Vk.FENCE_CREATE_SIGNALED_BIT
      } :: Vk.FenceCreateInfo '[]
