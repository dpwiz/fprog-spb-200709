module Resource.Object where

import Import

import Codec.Wavefront (WavefrontOBJ(..))

import qualified Codec.Wavefront as Obj
import qualified RIO.Text as Text
import qualified RIO.Vector as Vector
import qualified RIO.Vector.Partial as Vector

import qualified Geometry
import qualified Render.Simple.Vertex as Simple

data ResourceObjectError = ResourceObjectError Text
  deriving (Eq, Show)

instance Exception ResourceObjectError

load :: FilePath -> IO (Indexed Simple.Vertex)
load fp =
  Obj.fromFile fp >>= \case
    Left err ->
      throwM $ ResourceObjectError (Text.pack err)
    Right obj ->
      pure $ toIndexed obj
  where
    toIndexed WavefrontOBJ{..} = Geometry.indexed vertices
      where
        vertices = do
          Obj.Element{elValue=(Obj.Triangle a b c)} <- Vector.toList objFaces -- BUG: quads are ignored
          (loc, mtex, mnorm) <- do
            vertex <- [a, b, c]
            pure
              ( Obj.faceLocIndex vertex
              , Obj.faceTexCoordIndex vertex
              , Obj.faceNorIndex vertex
              )

          let Obj.Location x y z _w = objLocations Vector.! (loc - 1)

          let
            tc = case mtex of
              Nothing ->
                0
              Just ti ->
                V2 texcoordR (1 - texcoordS) -- XXX: texcoordT ignored
                where
                  Obj.TexCoord{..} = objTexCoords Vector.! (ti - 1)

          let
            normal = case mnorm of
              Nothing ->
                error $ "absent normal in " <> show fp -- TODO: ab x bc
              Just ti ->
                V3 norX norY norZ
                where
                  Obj.Normal{..} = objNormals Vector.! (ti - 1)

          pure Simple.Vertex
            { vPos      = V3 x y z
            , vColor    = 1
            , vTexCoord = tc
            , vNormal   = normal
            }
