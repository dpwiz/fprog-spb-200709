-- | JSON font loader for bitmaps and SDFs
--
-- Generator: https://evanw.github.io/font-texture-generator/
-- Usage (WebGL): https://evanw.github.io/font-texture-generator/example-webgl/

module Resource.Font.Evanw where

import RIO

import Data.Aeson (FromJSON, eitherDecodeFileStrict')
import Linear (V2(..))

import qualified RIO.HashMap as HashMap
import qualified RIO.Text as Text

data FontError = FontError Text
  deriving (Eq, Ord, Show, Generic)

instance Exception FontError

data Container = Container
  { name       :: Text
  , size       :: Float
  , bold       :: Bool
  , italic     :: Bool
  , width      :: Float
  , height     :: Float
  , characters :: HashMap Char Character
  }
  deriving (Eq, Ord, Show, Generic)

data Character = Character
  { x       :: Float
  , y       :: Float
  , width   :: Float
  , height  :: Float
  , originX :: Float
  , originY :: Float
  , advance :: Float
  }
  deriving (Eq, Ord, Show, Generic)

instance FromJSON Container

instance FromJSON Character

loadSprites :: MonadIO m => FilePath -> m Cached
loadSprites = fmap cacheSprites . liftIO . load

load :: FilePath -> IO Container
load fp = eitherDecodeFileStrict' fp >>= \case
  Left err ->
    throwIO $ FontError $ Text.pack err
  Right res ->
    pure res

data Cached = Cached
  { fontName :: Text
  , fontSize :: Float
  , sprites  :: HashMap Char Sprite
  }
  deriving (Eq, Ord, Show)

data Sprite = Sprite
  { size      :: V2 Float
  , texOffset :: V2 Float
  , texScale  :: V2 Float
  , origin    :: V2 Float
  , advance   :: Float
  }
  deriving (Eq, Ord, Show)

cacheSprites :: Container -> Cached
cacheSprites container = Cached
  { sprites  = HashMap.map toSprite characters
  , ..
  }
  where
    Container
      { width  = atlasWidth
      , height = atlasHeight
      , size   = fontSize
      , name   = fontName
      , characters
      } = container

    toSprite Character{..} = Sprite
      { size      = V2 width height
      , texScale  = V2 (width / atlasWidth) (height / atlasHeight)
      , texOffset = V2 (x / atlasWidth) (y / atlasHeight)
      , origin    = V2 originX originY
      , advance   = advance
      }

lookup :: Cached -> Char -> Maybe Sprite
lookup Cached{sprites} ch = HashMap.lookup ch sprites

toGlyphs :: Maybe Char -> Container -> Text -> [Either Float Character]
toGlyphs fallback Container{name, characters} = reverse . Text.foldl' f []
  where
    f acc ch =
      case ch of
        ' ' ->
          Left spaceAdvance : acc
        '\t' ->
          Left (spaceAdvance * 8) : acc
        '\n' ->
          Left spaceAdvance : acc -- TODO: line breaks
        _ ->
          case HashMap.lookup ch characters <|> unknownGlyph of
            Nothing ->
              acc
            Just character ->
              Right character : acc

    unknownGlyph = do
      ch <- fallback
      HashMap.lookup ch characters

    spaceMeasure = 'x'

    spaceAdvance =
      case HashMap.lookup spaceMeasure characters of
        Nothing ->
          error $ unwords
            [ "assert:"
            , show spaceMeasure
            , "is present in font"
            , show name
            ]
        Just Character{advance} ->
          advance
