{-# LANGUAGE Strict #-}

module Resource.Patat
  ( load
  , Presentation(..)
  ) where

import RIO

import Data.Functor.Foldable
import Data.Tree (Tree(..), drawForest)
import Geomancy.Mat4 (Mat4)
import Patat.Presentation (Presentation(..), readPresentation)
import Patat.Presentation.Internal (Slide(..), Fragment(..))
import RIO.FilePath
import Text.Pandoc.Definition
import Vulkan.Zero (Zero(..))
-- import Text.Pandoc.Walk

import qualified RIO.Text as Text

newtype PatatError = PatatError Text
  deriving (Eq, Ord, Show)

instance Exception PatatError

load :: FilePath -> IO Presentation
load path =
  readPresentation path >>= \case
    Left err ->
      throwIO $ PatatError (Text.pack err)
    Right pres -> do
      -- traverse dump $ map slideToScene (pSlides pres)
      pure pres

-- dump = \case
--   Left titleScene ->
--     Text.pack $ drawForest [dumpBlock titleScene]
--   Right contentFragments ->
--     Text.pack $ drawForest (map dumpBlock contentFragments)
--   where
--     dumpBlock = cata \case
--       RenderGroupF tr [] ->
--         Node
--           "Leaf"
--           []

--       RenderGroupF tr inner ->
--         Node
--           "Group"
--           [ Node "Transform" []
--           , Node "Children" inner
--           ]

--       RenderSpriteF fp ->
--         Node
--           "Sprite"
--           [ Node fp []
--           ]

--       RenderTextF ta t ->
--         Node
--           "Text"
--            [ Node "Style" [Node (show ta) []]
--            , Node (Text.unpack t) []
--            ]

--       RenderSolidF ->
--         Node
--           "Solid"
--           []

--       RenderModelF ->
--         Node
--           "Model"
--           []

-- loadScenes :: FilePath -> IO [Either Scene [Scene]]
-- loadScenes path = do
--   Presentation{..} <- load path
--   pure $ map slideToScene pSlides

-- slideToScene :: Slide -> Either Scene [Scene]
-- slideToScene = \case
--   TitleSlide block ->
--     Left $ blockToScene block
--   ContentSlide fragments ->
--     Right do
--       Fragment blocks <- fragments
--       pure $ RenderGroup zero (map blockToScene blocks)

-- type Transform = Fix TransformF

-- data TransformF f
--   = TranslateF Float Float Float f
--   | RotateF Float f
--   | ScaleF Float f
--   | IdentityF
--   deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

-- pattern Translate x y z next = Fix (TranslateF x y z next)
-- pattern Rotate t next = Fix (RotateF t next)
-- pattern Scale s next = Fix (ScaleF s next)

-- instance Zero Transform where
--   zero = Fix IdentityF

-- type Scene = Fix (RenderF Transform ())

-- data RenderF ga ta f
--   = RenderGroupF ga [f]
--   | RenderSpriteF FilePath
--   | RenderTextF ta Text
--   | RenderSolidF
--   | RenderModelF
--   deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

-- pattern RenderGroup :: Transform -> [Scene] -> Scene
-- pattern RenderGroup tr xs = Fix (RenderGroupF tr xs)

-- pattern RenderSprite :: FilePath -> Scene
-- pattern RenderSprite fp = Fix (RenderSpriteF fp)

-- pattern RenderText :: () -> Text -> Scene
-- pattern RenderText ta ts = Fix (RenderTextF ta ts)

-- pattern RenderSolid :: Scene
-- pattern RenderSolid = Fix (RenderSolidF)

-- pattern RenderModel :: Scene
-- pattern RenderModel = Fix (RenderModelF)

-- blockToScene :: Block -> Scene
-- blockToScene = \case
--   Null ->
--     RenderGroup zero []

--   Header level _attrs content ->
--     inlinesToScene content

--   Para content ->
--     inlinesToScene content

--   Plain content ->
--     inlinesToScene content

--   BulletList items ->
--     RenderGroup zero do
--       item <- items
--       pure $ RenderGroup zero
--         [ RenderSolid
--         , RenderGroup zero $ -- XXX: shift to right
--             map blockToScene item
--         ]

--   BlockQuote items ->
--     RenderGroup zero $ map blockToScene items

--   CodeBlock _attr t ->
--     RenderText (id mempty) t -- XXX: start with code style

--   todo ->
--     error $ "TODO: " <> show todo

-- inlinesToScene :: [Inline] -> Scene
-- inlinesToScene = compose ()
--   where
--     compose ta = \case
--       [] ->
--         RenderGroup zero []

--       Str t : next ->
--         RenderGroup zero
--           [ RenderText ta t
--           , compose ta next
--           ]

--       Code _attrs code : next ->
--         RenderGroup zero
--           [ RenderText (id ta) code -- XXX: a change of style
--           , compose ta next
--           ]

--       Span (_id, classes, pairs) inner : next ->
--         case classes of
--           ["emoji"] ->
--             case lookup "data-emoji" pairs of
--               Nothing ->
--                 error "unprocessed emoji"
--               Just emoji ->
--                 RenderGroup zero
--                   [ RenderSprite $
--                       "resources/textures/" </> Text.unpack emoji <.> "png"
--                   , compose (id ta) next -- XXX: needs image metrics
--                   ]

--       Strikeout inner : next ->
--         RenderGroup zero
--           [ RenderGroup zero
--               [ compose (id ta) inner
--               , RenderSolid -- XXX: needs inner metrics, overdraw
--               ]
--           , compose ta next
--           ]

--       Strong inner : next ->
--         RenderGroup zero
--           [ compose (id ta) inner -- XXX: a change of style
--           , compose ta next
--           ]

--       Emph inner : next ->
--         RenderGroup zero
--           [ compose (id ta) inner -- XXX: a change of style
--           , compose ta next
--           ]

--       Link _attrs inner _link : next ->
--         RenderGroup zero
--           [ compose (id ta) inner -- XXX: a change of style
--           , compose ta next
--           ]

--       Space : next ->
--         RenderGroup zero
--           [ compose ta next
--           ]

--       SoftBreak : next ->
--         RenderGroup zero -- XXX: drop a line? extract and convert to blocks?
--           [ compose ta next
--           ]

--       Image _attr alt (url, _name) : next ->
--         RenderGroup zero
--           [ RenderSprite $ Text.unpack url
--           , compose ta next -- XXX: needs image metrics
--           ]

--       todo : _rest ->
--         error $ "TODO: " <> show todo
