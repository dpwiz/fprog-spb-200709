module Resource.Image where

import Import

import Foreign (Ptr)

import qualified SDL
import qualified SDL.Image

loadWith :: (Integral size, MonadUnliftIO m) => FilePath -> (size -> size -> Ptr () -> m a) -> m a
loadWith filePath action =
  bracket (SDL.Image.load filePath) SDL.freeSurface $ \source -> do
    V2 width height <- SDL.surfaceDimensions source
    let sdlSize = V2 width height
    bracket (SDL.createRGBSurface sdlSize SDL.ABGR8888) SDL.freeSurface $ \temporary -> do
      _nothing <- SDL.surfaceBlit source Nothing temporary Nothing
      bracket_ (SDL.lockSurface temporary) (SDL.unlockSurface temporary) $ do
        pixels <- SDL.surfacePixels temporary
        action (fromIntegral width) (fromIntegral height) pixels
