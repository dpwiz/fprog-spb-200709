{-# LANGUAGE TemplateHaskell #-}

module Types where

import RIO

import Control.Lens.TH (makeLenses)
import Geomancy.Mat4 (Mat4)
import Linear (V2, V3, V4) -- , M44)
import RIO.Orphans (HasResourceMap(..), ResourceMap)
import RIO.Process (HasProcessContext(..), ProcessContext)

import qualified SDL
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Core11 as Vk11
import qualified Vulkan.Extensions.VK_KHR_surface as Khr
import qualified Vulkan.Extensions.VK_KHR_swapchain as Khr
import qualified VulkanMemoryAllocator as VMA

import Geometry.Types (SceneView)
import Game.World.Types (World)

import qualified Resource.Font.Evanw as FontB
-- import qualified Resource.Font.Proxima as FontS

-- | Command line arguments
data Options = Options
  { optionsVerbose    :: Bool
  , optionsFullscreen :: Bool
  , optionsWindowSize :: Maybe (V2 Int)
  , optionsValidation :: Bool
  , optionsRenderdoc  :: Bool
  , optionsFrameGC    :: Bool
  , optionsFrameDelay :: Bool
  } deriving (Show)

type AppSetup = App () ()
type AppGame = App GameContext GameState

data App context state = App
  { _appOptions        :: Options

  , _appLogFunc        :: LogFunc
  , _appProcessContext :: ProcessContext
  , _appResourceMap    :: ResourceMap

  , _appSdlWindow      :: SDL.Window
  , _appVulkanDevice   :: VulkanDevice

  , _appContext        :: context
  , _appState          :: SomeRef state
  }

-- | Initialized logical device and associated resources.
data VulkanDevice = VulkanDevice
  { _vdLogical        :: Vk.Device
  , _vdPhysical       :: PhysicalDevice
  , _vdSurface        :: Khr.SurfaceKHR
  , _vdSurfaceFormat  :: Vk.Format
  , _vdDepthFormat    :: Vk.Format
  , _vdGraphicsQ      :: Vk11.Queue
  , _vdPresentQ       :: Vk11.Queue
  , _vdAllocator      :: VMA.Allocator
  , _vdCommandPool    :: Vk.CommandPool
  , _vdSyncObjects    :: Vector SyncObject
  , _vdSamplers       :: Vector Vk.Sampler
  }
  deriving (Show)

-- | Physical device annotated with extracted properties.
data PhysicalDevice = PhysicalDevice
  { _pdPhysicalDevice    :: Vk.PhysicalDevice
  , _pdProperties        :: Vk.PhysicalDeviceProperties
  , _pdFeatures          :: Vk.PhysicalDeviceFeatures
  , _pdMsaaSamples       :: Vk.SampleCountFlagBits
  , _pdSamplerAnisotropy :: Float
  , _pdGraphicsQueueIx   :: Word32
  , _pdPresentQueueIx    :: Word32
  , _pdPresentMode       :: Khr.PresentModeKHR
  , _pdSurfaceFormat     :: Khr.SurfaceFormatKHR
  , _pdSurfaceCaps       :: Khr.SurfaceCapabilitiesKHR
  , _pdMemory            :: Vk.PhysicalDeviceMemoryProperties
  }
  deriving (Show)

-- XXX: recreated on e.g. window resize, file reload
data GameContext = GameContext
  { _gcVulkan    :: VulkanContext

  , _gcSimple       :: PipelineContext
  , _gcSimpleRender :: RenderContext
  -- , _gcRoom    :: RenderContext
  -- , _gcHgj     :: RenderContext
  -- , _gcDavid   :: RenderContext
  -- , _gcFashion :: RenderContext
  -- , _gcCd      :: RenderContext

  , _gcSolid       :: PipelineContext
  , _gcSolidRender :: RenderContext

  , _gcSprite       :: PipelineContext
  , _gcSpriteRender :: RenderContext

  , _gcSdf       :: PipelineContext
  , _gcSdfRender :: RenderContext
  }
  deriving (Show)

-- | Vulkan bits that change through the window lifetime but don't depend on rendering details.
data VulkanContext = VulkanContext
  { _vcExtent         :: Vk.Extent2D
  , _vcRenderPass     :: Vk.RenderPass
  , _vcDescriptorPool :: Vk.DescriptorPool
  , _vcSwapChain      :: Khr.SwapchainKHR

  -- XXX: per-swapchain image resources
  , _vcSwapLength     :: Int
  , _vcSwapViews      :: Vector Vk.ImageView
  , _vcCommandBuffers :: Vector Vk.CommandBuffer
  , _vcFramebuffers   :: Vector Vk.Framebuffer
  }
  deriving (Show)

-- | Per-image frame pipeline sync objects
data SyncObject = SyncObject
  { _soImageAvailable :: Vk.Semaphore
  , _soRenderFinished :: Vk.Semaphore
  , _soFramesInFlight :: Vk.Fence
  }
  deriving (Show)

data PipelineContext = PipelineContext
  { _pcPipeline :: Vk.Pipeline
  , _pcLayout   :: Vk.PipelineLayout
  }
  deriving (Eq, Ord, Show)

data RenderContext = RenderContext
  { _rcDescSets  :: Vector Vk.DescriptorSet
    -- XXX: set0 bindings
  , _rcScene     :: Allocated Vk.Buffer SceneView
  , _rcObject    :: Allocated Vk.Buffer Mat4
  }
  deriving (Show)

-- XXX: persists between context changes, available as MonadState
data GameState = GameState
  { _gsQuit          :: Bool
  , _gsUpdateContext :: Bool

  , _gsWorld :: World

  , _gsFontCopy   :: FontB.Cached
  , _gsFontHeader :: FontB.Cached
  -- , _gsFontS :: FontS.Container

  , _gsRoom   :: Model
  , _gsCube   :: Model
  , _gsSprite :: Model
  , _gsMesh   :: Model

  , _gsTextures   :: Vector Vk.ImageView
  , _gsTextureIds :: Map FilePath Int
  }

data Allocated buf a = Allocated
  { _allocated      :: buf
  , _allocation     :: VMA.Allocation
  , _allocationInfo :: VMA.AllocationInfo
  }
  deriving (Show)

type Vec2 = V2 Float
type Vec3 = V3 Float
type Vec4 = V4 Float

-- | On-device representation of indexed vertices and textures.
data Model = Model
  { _modelVertices   :: Vk.Buffer
  , _modelIndices    :: Vk.Buffer
  , _modelIndexCount :: Word32
  -- , _modelTexture    :: Vk.ImageView
  , _modelTransform  :: Mat4
  }
  deriving (Show)

-- XXX: Add context-bound descriptor set for models?

-- | Compiled shader IR.
newtype SpirV = SpirV { unSpirV :: ByteString }

data ShaderStages = ShaderStages
  { _ssVertex   :: Vk.ShaderModule
  , _ssFragment :: Vk.ShaderModule
  }

makeLenses ''App
makeLenses ''VulkanDevice
makeLenses ''PhysicalDevice

makeLenses ''GameContext
makeLenses ''VulkanContext
makeLenses ''RenderContext

makeLenses ''GameState

makeLenses ''Model

instance HasLogFunc (App context state) where
  logFuncL = appLogFunc

instance HasProcessContext (App context state) where
  processContextL = appProcessContext

instance HasResourceMap (App context state) where
  resourceMapL = appResourceMap

instance HasStateRef state (App context state) where
  stateRefL = appState
