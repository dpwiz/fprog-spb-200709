module Game.Events where

import Import

import qualified Linear
import qualified SDL

handle :: SDL.Event -> RIO AppGame ()
handle event = do
  -- traceShowM ("Game.Events.handle", event)
  case SDL.eventPayload event of
    SDL.KeyboardEvent kb ->
      handleKeyboard kb
    SDL.MouseButtonEvent mb ->
      handleMouseButton mb
    SDL.QuitEvent ->
      gsQuit .= True
    _ ->
      pure ()

handleKeyboard :: SDL.KeyboardEventData -> RIO AppGame ()
handleKeyboard SDL.KeyboardEventData{keyboardEventKeyMotion=SDL.Released} = pure ()
handleKeyboard SDL.KeyboardEventData{..} =
  case SDL.keysymKeycode keyboardEventKeysym of
    SDL.KeycodeEscape ->
      gsQuit .= True

    SDL.Keycode1 | not keyboardEventRepeat && keyboardEventKeyMotion == SDL.Pressed ->
      gsWorld . worldShowModels %= not

    SDL.Keycode2 | not keyboardEventRepeat && keyboardEventKeyMotion == SDL.Pressed ->
      gsWorld . worldShowSolids %= not

    SDL.Keycode3 | not keyboardEventRepeat && keyboardEventKeyMotion == SDL.Pressed ->
      gsWorld . worldShowSprites %= not

    SDL.Keycode4 | not keyboardEventRepeat && keyboardEventKeyMotion == SDL.Pressed ->
      gsWorld . worldShowSlides %= not

    SDL.KeycodeA ->
      gsWorld . worldCamera . cameraOrigin %=
        Linear.rotate (Linear.axisAngle (V3 0 1 0) 0.1)

    SDL.KeycodeD ->
      gsWorld . worldCamera . cameraOrigin %=
        Linear.rotate (Linear.axisAngle (V3 0 (-1) 0) 0.1)

    SDL.KeycodeW ->
      gsWorld . worldCamera %= \cam -> cam
        & cameraOrigin %~ Linear.lerp 0.1 (cam ^. cameraTarget)

    SDL.KeycodeS ->
      gsWorld . worldCamera %= \cam -> cam
        & cameraOrigin %~ Linear.lerp (-0.1) (cam ^. cameraTarget)

    SDL.KeycodeQ ->
      gsWorld . worldCamera %= \cam -> cam
        & cameraOrigin . Linear._y +~ 0.1
        & cameraTarget . Linear._y +~ 0.1

    SDL.KeycodeE ->
      gsWorld . worldCamera %= \cam -> cam
        & cameraOrigin . Linear._y -~ 0.1
        & cameraTarget . Linear._y -~ 0.1

    SDL.KeycodeSpace ->
      gsWorld . worldSlideFlip %= not

    SDL.KeycodeLeft ->
      changeSlide $ Left prevSlide

    SDL.KeycodeRight ->
      changeSlide $ Left nextSlide

    SDL.KeycodeHome ->
      changeSlide $ Right minBound

    SDL.KeycodeEnd ->
      changeSlide $ Right maxBound

    _ ->
      pure ()

handleMouseButton :: SDL.MouseButtonEventData -> RIO AppGame ()
handleMouseButton SDL.MouseButtonEventData{..} = case mouseButtonEventButton of
  SDL.ButtonRight ->
    case mouseButtonEventMotion of
      SDL.Pressed ->
        gsWorld . worldSlideFlip .= True
      SDL.Released ->
        gsWorld . worldSlideFlip .= False

  SDL.ButtonX1 | mouseButtonEventMotion == SDL.Pressed ->
    changeSlide $ Left prevSlide

  SDL.ButtonX2 | mouseButtonEventMotion == SDL.Pressed ->
    changeSlide $ Left nextSlide

  _ ->
    -- traceShowM b
    pure ()

handleTime :: Integer -> Integer -> RIO AppGame ()
handleTime prevTime startTime = do
  tickOverlay deltaSeconds
  where
    deltaSeconds :: Float
    deltaSeconds = fromIntegral (startTime - prevTime) * 1e-11

changeSlide :: Either (SlideCur -> SlideCur) SlideCur -> RIO AppGame ()
changeSlide update = gsWorld %= batchUpdate
  where
    batchUpdate world =
      if next == cur then
        world
      else
        world
          { _worldSlidePrev    = (cur, flipped)
          , _worldSlideCur     = next
          , _worldSlideFlip    = False
          , _worldOverlayTimer = OVERLAY_TIME
          }
      where
        cur     = _worldSlideCur world
        flipped = _worldSlideFlip world

        next = case update of
          Left tr -> tr cur
          Right s -> s

tickOverlay :: Float -> RIO AppGame ()
tickOverlay deltaSeconds =
  gsWorld . worldOverlayTimer %= \old ->
    max 0 (old - deltaSeconds)
