{-# LANGUAGE TemplateHaskell #-}

module Game.World.Types
  ( World(..)
  , worldCamera
  , worldShowModels
  , worldShowSolids
  , worldShowSprites
  , worldShowSlides
  , worldSlideCur
  , worldSlideFlip
  , worldSlidePrev
  , worldOverlayTimer
  , pattern OVERLAY_TIME

  , Camera(..)
  , cameraOrigin
  , cameraTarget
  , cameraFov

  , SlideCur(..)
  , prevSlide
  , nextSlide
  , JamCur(..)
  , FoomCur(..)
  , SpacemarCur(..)
  , Playground2Cur(..)
  , ExstareCur(..)
  , Playground3Cur(..)
  ) where

import RIO

import Control.Lens.TH (makeLenses)
import Linear (V3)
import RIO.Partial (pred, succ)

pattern OVERLAY_TIME :: Float
pattern OVERLAY_TIME = 0.25

data World = World
  { _worldCamera :: Camera

  , _worldShowSolids  :: Bool
  , _worldShowModels  :: Bool
  , _worldShowSprites :: Bool
  , _worldShowSlides  :: Bool

  , _worldSlideCur     :: SlideCur
  , _worldSlideFlip    :: Bool
  , _worldSlidePrev    :: (SlideCur, Bool)
  , _worldOverlayTimer :: Float
  }

data Camera = Camera
  { _cameraOrigin :: V3 Float
  , _cameraTarget :: V3 Float
  , _cameraFov    :: Float
  }

data SlideCur
  = Cover
  | Jam         JamCur
  | Foom        FoomCur
  | Spacemar    SpacemarCur
  | Playground2 Playground2Cur
  | Exstare     ExstareCur
  | Playground3 Playground3Cur
  | Final
  deriving (Eq, Ord, Show)

nextSlide :: SlideCur -> SlideCur
nextSlide = \case
  Cover           -> Jam minBound
  Jam cur         -> rollNext Jam         cur (Foom minBound)
  Foom cur        -> rollNext Foom        cur (Spacemar minBound)
  Spacemar cur    -> rollNext Spacemar    cur (Playground2 minBound)
  Playground2 cur -> rollNext Playground2 cur (Exstare minBound)
  Exstare cur     -> rollNext Exstare     cur (Playground3 minBound)
  Playground3 cur -> rollNext Playground3 cur Final
  Final           -> Final

prevSlide :: SlideCur -> SlideCur
prevSlide = \case
  Cover           -> Cover
  Jam cur         -> rollPred Jam         cur Cover
  Foom cur        -> rollPred Foom        cur (Jam maxBound)
  Spacemar cur    -> rollPred Spacemar    cur (Foom maxBound)
  Playground2 cur -> rollPred Playground2 cur (Spacemar maxBound)
  Exstare cur     -> rollPred Exstare     cur (Playground2 maxBound)
  Playground3 cur -> rollPred Playground3 cur (Exstare maxBound)
  Final           -> Playground3 maxBound

rollPred :: (Eq t, Bounded t, Enum t) => (t -> p) -> t -> p -> p
rollPred cons x roll =
  if x == minBound then
    roll
  else
    cons (pred x)

rollNext :: (Eq t, Bounded t, Enum t) => (t -> p) -> t -> p -> p
rollNext cons x roll =
  if x == maxBound then
    roll
  else
    cons (succ x)

instance Bounded SlideCur where
  minBound = Cover
  maxBound = Final

data JamCur
  = Haskarium
  | HaskellGameJam1
  | HaskellGameJam2
  deriving (Eq, Ord, Show, Enum, Bounded)

data FoomCur
  = MissileCommand
  | Foom1
  | Foom2
  | ApecsStarter1
  | ApecsStarter2
  | FoomItch
  | FoomFindings
  deriving (Eq, Ord, Show, Enum, Bounded)

data SpacemarCur
  = SpacemarCover
  | GamesMadeQuick
  | Spacewar1
  | Spacewar2
  | SpacemarDog
  | SpacemarJam
  | SpacemarResults
  | SpacemArt
  | ObservationLounge
  | FieldExperiments
  | SpacemarFindings
  deriving (Eq, Ord, Show, Enum, Bounded)

data Playground2Cur
  = Playground2Cover
  | LudumDare46
  | PreJam1
  | PreJam2
  deriving (Eq, Ord, Show, Enum, Bounded)

data ExstareCur
  = ExstareCover
  | Exstare1
  | Exstare2
  | Exstare3
  | ExstareFindings
  deriving (Eq, Ord, Show, Enum, Bounded)

data Playground3Cur
  = Playground3Cover
  | VulkanHello
  | VulkanTutorial
  | Playground3Then
  | Playground3Now
  | Playground3Findings
  deriving (Eq, Ord, Show, Enum, Bounded)

makeLenses ''World
makeLenses ''Camera
