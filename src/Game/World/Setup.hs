module Game.World.Setup
  ( setup
  ) where

import Import

setup :: MonadIO m => m World
setup =
  pure World
    { _worldCamera = Camera
        { _cameraOrigin = V3 5 (-2) (-3)
        , _cameraTarget = V3 0 0 0
        , _cameraFov    = 45
        }
    , _worldShowModels  = not True
    , _worldShowSolids  = True
    , _worldShowSprites = not True
    , _worldShowSlides  = True

    , _worldSlideCur     = minBound
    , _worldSlideFlip    = False
    , _worldSlidePrev    = (minBound, False)
    , _worldOverlayTimer = 0
    }
