{-# LANGUAGE TemplateHaskell #-}

module Game.Static.Models
  ( module Game.Static.Models
  ) where

import qualified Resource.TH as TH

TH.filePaths "resources/models"
TH.filePatterns "resources/models"
