{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -fforce-recomp #-}

module Game.Static.Textures
  ( module Game.Static.Textures
  ) where

import qualified Resource.TH as TH

TH.filePaths "resources/textures"
TH.filePatterns "resources/textures"
