{-# LANGUAGE TemplateHaskell #-}

module Game.Static.Fonts
  ( module Game.Static.Fonts
  ) where

import qualified Resource.TH as TH

TH.filePaths "resources/fonts"
TH.filePatterns "resources/fonts"
