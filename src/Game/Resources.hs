module Game.Resources
  ( setup
  , acquire
  ) where

import Import

import Data.Acquire (Acquire)

import qualified Geomancy.Mat4 as Mat4
import qualified SDL
import qualified RIO.Vector as Vector
import qualified RIO.Map as Map
import qualified RIO.List as List

import qualified Game.Static.Fonts as Fonts
import qualified Game.Static.Models as Models
import qualified Game.Static.Textures as Textures
import qualified Game.World.Setup as World
import qualified Render.Model as Model
import qualified Render.Simple.Context as Simple
import qualified Render.Solid.Context as Solid
import qualified Render.SDF.Context as SDF
import qualified Render.Sprite.Context as Sprite
import qualified Render.Texture as Texture
import qualified Resource.Font.Evanw as FontB
-- import qualified Resource.Font.Proxima as FontS
import qualified Vulkan.Context as Vulkan

setup :: VulkanDevice -> Acquire GameState
setup vd = do
  world <- World.setup

  fontCopy   <- FontB.loadSprites Fonts.UBUNTU_24_JSON
  fontHeader <- FontB.loadSprites Fonts.ROBOTO_MEDIUM_64_JSON

  mesh   <- Model.acquireGltf vd Models.VIKING_ROOM_GLTF $ Mat4.rotateX pi -- XXX: vulkan is -Y up
  room   <- Model.acquireObj vd Models.VIKING_ROOM_OBJ $ Mat4.rotateX (-pi/2) -- XXX: model lies on its side o_O
  cube   <- Model.acquireObj vd Models.CUBE_OBJ mempty
  sprite <- Sprite.acquireModel vd

  filler <- Texture.acquire vd Textures.BLACK8_PNG
  textures <- for (zip [1..] Textures.paths) \(ix, fp) -> do
    imageView <- Texture.acquire vd fp
    pure (Map.singleton fp ix, imageView)
  let (textureIds, textureViews) = List.unzip textures

  pure GameState
    { _gsQuit          = False
    , _gsUpdateContext = False

    , _gsWorld = world

    , _gsFontCopy   = fontCopy
    , _gsFontHeader = fontHeader

    , _gsRoom   = room
    , _gsCube   = cube
    , _gsMesh   = mesh
    , _gsSprite = sprite

    , _gsTextures   = Vector.fromList $ filler : textureViews
    , _gsTextureIds = Map.insert "" 0 $ mconcat textureIds
    }

acquire :: SDL.Window -> VulkanDevice -> GameState -> Acquire GameContext
acquire window dev GameState{..} = do
  _gcVulkan <- Vulkan.acquire window dev

  (_gcSimple, _gcSimpleRender) <- Simple.acquire dev _gcVulkan _gsTextures
  (_gcSolid, _gcSolidRender)   <- Solid.acquire dev _gcVulkan
  (_gcSprite, _gcSpriteRender) <- Sprite.acquire dev _gcVulkan _gsTextures
  (_gcSdf, _gcSdfRender)       <- SDF.acquire dev _gcVulkan _gsTextures

  pure GameContext{..}
