module Game.Draw.Text where

import Import

import Data.Acquire (Acquire, allocateAcquire)

import qualified Geomancy.Mat4 as Mat4
import qualified RIO.Map as Map
import qualified RIO.Text as Text
import qualified SDL
import qualified Vulkan.Core10 as Vk

import Geometry.Projection (orthographic)
import Render.Draw (Draw(..))

import qualified Game.Static.Textures as Textures
import qualified Render.Model as Model
import qualified Render.Setup.Context as Context
import qualified Render.SDF.Context as SDF
import qualified Render.SDF.Vertex as SDF
import qualified Render.Sprite.Context as Sprite
-- import qualified Resource.Font.Evanw as Font
import qualified Resource.Font.Proxima as Font

data Chunk a = Chunk
  { chunkData      :: a
  , chunkTransform :: Mat4
  , chunkStyle     :: Texture -> Texture
  }

styled_ :: (Texture -> Texture) -> a -> Chunk a
styled_ style x = Chunk
  { chunkData      = x
  , chunkTransform = mempty
  , chunkStyle     = style
  }

coloredFF_ :: Word32 -> a -> Chunk a
coloredFF_ rgb = styled_ $ tBase .~ V4 rf gf bf 1
  where
    rf = fromIntegral r / 0xFF
    gf = fromIntegral g / 0xFF
    bf = fromIntegral b / 0xFF

    r = 0xFF .&. rgb
    g = 0xFF .&. shiftR rgb 8
    b = 0xFF .&. shiftR rgb 16

-- TODO
backgroundFFA_ :: Word32 -> Chunk a -> Chunk a
backgroundFFA_ _rgba = id

-- TODO
bold_ :: Chunk a -> Chunk a
bold_ = id

-- * QuietLight theme

-- https://github.com/onecrayon/theme-quietlight-vsc/blob/master/themes/QuietLight.json

plain_ :: a -> Chunk a
plain_ = coloredFF_ 0x000000

comment_ :: a -> Chunk a
comment_ = coloredFF_ 0xaaaaaa

commentKeyword_ :: a -> Chunk a
commentKeyword_ = bold_ . comment_

invalid_ :: a -> Chunk a
invalid_ = backgroundFFA_ 0x96000014 . coloredFF_ 0x660000

operator_ :: a -> Chunk a
operator_ = coloredFF_ 0x777777

keyword_ :: a -> Chunk a
keyword_ = coloredFF_ 0x4b83cd

keywordPunctuation_ :: a -> Chunk a
keywordPunctuation_ = coloredFF_ 0x91b3e0

type_ :: a -> Chunk a
type_ = coloredFF_ 0x7a3e9d

constant_ :: a -> Chunk a
constant_ = coloredFF_ 0xab6526

variable_ :: a -> Chunk a
variable_ = coloredFF_ 0x7a3e9d -- XXX: same as type?

-- XXX: wat
variablePunctuation_ :: a -> Chunk a
variablePunctuation_ = coloredFF_ 0xb19bbd

functionDefinition_ :: a -> Chunk a
functionDefinition_ = bold_ . plain_

number_ :: a -> Chunk a
number_ = coloredFF_ 0xab6526

string_ :: a -> Chunk a
string_ = coloredFF_ 0x448c27

stringEscape_ :: a -> Chunk a
stringEscape_ = coloredFF_ 0xab6526 -- XXX: same as number?

-- -- * Alabaster theme

-- plain_ :: a -> Chunk a
-- plain_ = coloredFF_ 0x000000

-- comment_ :: a -> Chunk a
-- comment_ = coloredFF_ 0xAA3731

-- string_ :: a -> Chunk a
-- string_ = coloredFF_ 0x448C27

-- stringEscape_ :: a -> Chunk a
-- stringEscape_ = coloredFF_ 0x777777

-- literal_ :: a -> Chunk a
-- literal_ = coloredFF_ 0x7A3E9D

-- global_ :: a -> Chunk a
-- global_ = coloredFF_ 0x325CC0

-- punctuation_ :: a -> Chunk a
-- punctuation_ = coloredFF_ 0x777777

-- invalid_ :: a -> Chunk a
-- invalid_ = coloredFF_ 0x660000

textBlock :: VulkanDevice -> Font.Container -> [[Chunk Text]] -> Acquire [Chunk Model]
textBlock vd font = fmap fst . foldl' foldLine (pure (mempty, 0))
  where
    foldLine before chunks = do
      (previous, offsetY) <- before
      (models, _width) <- foldl' (foldChunks offsetY) (pure (mempty, 0)) chunks
      pure
        ( models <> previous
        -- , offsetY - Font.size font * 1.1
        , offsetY - 64 * 1.2
        )

    foldChunks offsetY before Chunk{..} = do
      (previous, offsetX) <- before
      (model, advanceX) <- textProxima vd font chunkData
      pure
        ( Chunk
            { chunkData      = model
            , chunkTransform = chunkTransform <> Mat4.translate offsetX offsetY 0
            , chunkStyle
            } : previous
        , offsetX + advanceX
        )

textProxima :: VulkanDevice -> Font.Container -> Text -> Acquire (Model, Float)
textProxima vd font line =
  (,)
    <$> Model.acquireIndexed vd indexed
    <*> pure (sumAdvance $ Text.length line)
  where
    indexed = mconcat do
      (ix, Right (_glyphAdvance, baseVertices)) <- zip [0..] glyphs
      let
        baseX = sumAdvance ix

        vertices = do
            V4 x y s t <- baseVertices
            pure SDF.Vertex
              { vPos      = V2 (baseX + x) y
              , vTexCoord = V2 s t
              }
      pure $ Indexed vertices Font.indices

    glyphs = Font.toGlyphs (Just ' ') font line

    sumAdvance ix =
      sum $ map (either id fst) $ take ix glyphs
