module Game.Draw.Solids where

import Import

import Data.Acquire (allocateAcquire)
import Data.List (zip3)

import qualified Geomancy.Mat4 as Mat4
import qualified Linear
import qualified SDL
import qualified Vulkan.Core10 as Vk

import Geometry.Projection (looking, perspective)
import Render.Draw (Draw(..))

import qualified Geometry.Solids as Solids
import qualified Render.Model as Model
import qualified Render.Setup.Context as Context
import qualified Render.Solid.Context as Solid
import qualified Render.Solid.Vertex as Solid

makeSolids :: RIO AppGame Draw
makeSolids = do
  ticks <- SDL.ticks
  let seconds = fromIntegral ticks / 1000

  -- XXX: Fetch game resources
  vd <- view appVulkanDevice
  GameState{..} <- use id
  GameContext{..} <- view appContext

  let PipelineContext{..} = _gcSolid
  let RenderContext{..} = _gcSolidRender

  -- XXX: Set up camera
  Camera{..} <- use $ gsWorld . worldCamera
  extent <- view (appContext . gcVulkan . vcExtent)

  Context.setScene
    SceneView
      { _svView       = looking _cameraOrigin _cameraTarget
      , _svProjection = perspective extent _cameraFov (-0.5) 1000
      , _svViewPos    = _cameraOrigin
      , _svTime       = seconds
      }
    _gcSolidRender

  let
    indexed = rgbPyramid 1.0 <> ribbon 2 32

    rgbPyramid size = Indexed colored indices
      where
        Indexed vertices indices = Solids.tetrahedron size

        colored = zipWith Solid.Vertex colors vertices

        colors =
          [ V4 a a a 1
          , V4 1 0 0 1
          , V4 0 1 0 1
          , V4 0 0 1 1
          ]
        a = 0.5 + 0.5 * sin seconds

    ribbon turns size = Indexed items (indices <> reverse indices)
      where
        items = do
          (ix, (color, pos), (_nextColor, nextPos)) <- zip3 [0..] path (drop 1 path)
          let axis = pos - nextPos
          let q = Linear.axisAngle axis (pi * ix / fromIntegral size / fromIntegral turns)

          id
            [ Solid.Vertex color $ pos + Linear.rotate q (V3 (-0.25) 0 0)
            , Solid.Vertex color $ pos + Linear.rotate q (V3 ( 0.25) 0 0)
            ]

        path = do
          ix <- [0 :: Int .. turns * size + 1]
          let
            x = fromIntegral turns * pi * fromIntegral ix / fromIntegral size
            fx = sin (x + 0.1 * seconds)
            gx = cos (x + 0.1 * seconds)

            color = V4 (0.5 + 0.5 * fx) (0.5 + 0.5 * gx) 0.5 1.0

          pure (color, V3 ( (x - 6) / fromIntegral turns) gx fx)

        indices = concatMap strip [0 .. len - 2]
          where
            len = fromIntegral $ length items `div` 2

            strip i = map (+ 2*i)
              [ 0, 1, 3
              , 0, 3, 2
              ]

  (solidRelease, solidModel) <- allocateAcquire $ Model.acquireIndexed vd indexed

  let resources = [solidRelease]

  pure $ DrawCommands resources \cmd -> do
    let Model{..} = solidModel

    Vk.cmdBindPipeline cmd Vk.PIPELINE_BIND_POINT_GRAPHICS _pcPipeline
    Vk.cmdBindDescriptorSets cmd Vk.PIPELINE_BIND_POINT_GRAPHICS _pcLayout 0 _rcDescSets mempty

    Vk.cmdBindVertexBuffers cmd 0 (pure _modelVertices) (pure 0)
    Vk.cmdBindIndexBuffer cmd _modelIndices 0 Vk.INDEX_TYPE_UINT32

    Solid.pushVertex cmd _pcLayout $ mconcat
      [ Mat4.rotateZ (-seconds / pi)
      ]
    let
      instanceCount = 1
      firstIndex    = 0
      vertexOffset  = 0
      firstInstance = 0
    Vk.cmdDrawIndexed cmd _modelIndexCount instanceCount firstIndex vertexOffset firstInstance
