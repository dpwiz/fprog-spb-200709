module Game.Draw.Models where

import Import

import Data.Acquire (allocateAcquire, mkAcquire)

import qualified Foreign
import qualified Geomancy.Mat4 as Mat4
import qualified Linear
import qualified RIO.List as List
import qualified RIO.Map as Map
import qualified RIO.Vector as Vector
import qualified SDL
import qualified Vulkan.Core10 as Vk
import qualified VulkanMemoryAllocator as VMA

import Geometry.Projection (looking, perspective)
import Render.Draw (Draw(..))
import Render.Scene (flatten, leaf, node, unfoldForest)

import qualified Game.Static.Textures as Textures
import qualified Render.Setup.Context as Context
import qualified Render.Simple.Context as Simple

makeScene :: RIO AppGame Draw
makeScene = do
  ticks <- SDL.ticks
  let seconds = fromIntegral ticks / 1000

  -- XXX: Fetch game resources
  -- vd <- view appVulkanDevice
  GameState{..} <- use id
  GameContext{..} <- view appContext

  let PipelineContext{..} = _gcSimple

  -- XXX: Set up camera
  Camera{..} <- use $ gsWorld . worldCamera
  extent <- view (appContext . gcVulkan . vcExtent)
  Context.setScene
    SceneView
      { _svView       = looking _cameraOrigin _cameraTarget
      , _svProjection = perspective extent _cameraFov (-0.5) 1000
      , _svViewPos    = _cameraOrigin + Linear.normalize (_cameraOrigin - _cameraTarget) Linear.^* 0.5
      , _svTime       = seconds
      }
    _gcSimpleRender

  let
    material key = case Map.lookup key _gsTextureIds of
      Nothing ->
        error "assert: all static textures are indexed"
      Just ix ->
        Texture
          { _tBase      = 1.0
          , _tOffset    = 0
          , _tScale     = 1
          , _tSamplerId = 0
          , _tImageId   = fromIntegral ix
          }
  let
    _cubeForest = unfoldForest grow [V3 0 0 0]
      where
        grow :: Vec3 -> ((Mat4, Maybe Texture), [Vec3])
        grow (V3 x y z) =
          ( ( Mat4.rotateX (sin $ seconds / tau) <> Mat4.translate x (-2 + 0.1 * cos seconds) z
            , Just $ material Textures.KINDA_EARTH_JPG
            )
          , if y <= 5 then
              [ V3 (x + 1.1) (y + 1) z
              , V3 x       (y + 1) (z + 1.1)
              ]
            else
              []
          )

  let
    groups =
      [ ( _gsRoom
        , [ leaf
              (Mat4.rotateY (-pi/2))
              (material Textures.VIKING_ROOM_PNG)
          ]
        )
      , ( _gsMesh
        , [ leaf
              ( Mat4.scale 0.5 0.5 0.5 <>
                Mat4.translate 0 5 0
              )
              (material Textures.VIKING_ROOM_PNG)
          ]
        )
      , ( _gsCube
        , [ node
              ( mconcat $
                [ Mat4.scale 0.2 0.2 0.2
                , Mat4.rotateX (0.2 * seconds)
                , Mat4.rotateY (0.3 * seconds)
                , Mat4.rotateZ (0.5 * seconds)
                , Mat4.translate 0 (-0.5) 0
                ]
              )
              (material Textures.KINDA_EARTH_JPG) $
                let
                  r = 0.5 + 0.5 * sin seconds
                  s = r * 0.5
                in
                  [ leaf
                      (Mat4.scale s s s <> Mat4.translate 0 r 0)
                      (material Textures.DAVID_JPG)
                  , leaf
                      (Mat4.scale s s s <> Mat4.translate 0 (-r) 0)
                      (material Textures.FASHION_JPG)
                  ]
          -- , node_ mempty _cubeForest
          ]
        )
      ]

  vma <- view (appVulkanDevice . vdAllocator)
  allocated <- for groups \(model, tree) -> do
    let
      flat = concatMap flatten tree
      numInstances = length flat -- XXX: must not be 0

      (perInstance, indexedDraws) = List.unzip do
        (ix, (vert, frag)) <- zip [0..] flat
        pure
          ( _modelTransform model <> vert
          , (ix, frag)
          )

      perDraw = do
        batch@((firstInstance, frag) : _rest) <- List.groupBy ((==) `on` snd) indexedDraws
        pure
          ( firstInstance
          , fromIntegral $ length batch
          , frag
          )

      ci = zero
        { Vk.size        = fromIntegral $ numInstances * Foreign.sizeOf @Mat4 mempty
        , Vk.usage       = Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
        , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
        }

      ai = zero
        { VMA.usage =
            VMA.MEMORY_USAGE_GPU_ONLY
        , VMA.requiredFlags =
            Vk.MEMORY_PROPERTY_HOST_VISIBLE_BIT .|.
            Vk.MEMORY_PROPERTY_HOST_COHERENT_BIT -- TODO: use aggregated flush
        }

    -- XXX: memory is mapped until post-draw release
    (release, mappedBuf) <- allocateAcquire $ do
      (buf, allocation, _ai) <- VMA.withBuffer vma ci ai mkAcquire
      ptr <- VMA.withMappedMemory vma allocation mkAcquire
      liftIO $ Foreign.pokeArray (Foreign.castPtr ptr) perInstance
      pure buf

    pure
      ( model
      , ( release
        , ( mappedBuf
          , perDraw
          )
        )
      )

  let
    (transient, groups2) = List.unzip do
      (model, (resources, items)) <- allocated
      pure (resources, (model, items))

  pure $ DrawCommands transient \cmd -> do
    Vk.cmdBindPipeline cmd Vk.PIPELINE_BIND_POINT_GRAPHICS _pcPipeline
    Vk.cmdBindDescriptorSets cmd Vk.PIPELINE_BIND_POINT_GRAPHICS _pcLayout 0 (_rcDescSets _gcSimpleRender) mempty

    for_ groups2 \(Model{..}, (perInstance, perDraw)) -> do
      Vk.cmdBindVertexBuffers cmd 0
        (Vector.fromList [_modelVertices, perInstance])
        (Vector.fromList [0, 0])
      Vk.cmdBindIndexBuffer cmd _modelIndices 0 Vk.INDEX_TYPE_UINT32

      for_ perDraw \(firstInstance, instanceCount, frag) -> do
        -- Simple.pushVertex cmd _pcLayout mempty
        Simple.pushFragment cmd _pcLayout $ Simple.FragmentPush frag

        let
          firstIndex   = 0
          vertexOffset = 0
        Vk.cmdDrawIndexed cmd _modelIndexCount instanceCount firstIndex vertexOffset firstInstance
