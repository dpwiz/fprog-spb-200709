module Game.Draw.Slides where

import Import

import qualified Geomancy.Mat4 as Mat4
import qualified RIO.Map as Map
import qualified SDL
import qualified Vulkan.Core10 as Vk

import Geometry.Projection (orthographic)
import Render.Draw (Draw(..))

import qualified Game.Static.Textures as Textures
import qualified Render.Setup.Context as Context
import qualified Render.Sprite.Context as Sprite
import qualified Resource.Font.Evanw as FontB

makeSlides :: RIO AppGame Draw
makeSlides = do
  ticks <- SDL.ticks
  let seconds = fromIntegral ticks / 1000

  World{..} <- use gsWorld
  let transient = []

  -- XXX: Fetch game resources
  GameState{..} <- use id
  GameContext{..} <- view appContext


  extent <- view (appContext . gcVulkan . vcExtent)
  let
    sv =
      SceneView
        { _svView       = mempty
        , _svProjection = orthographic extent
        , _svViewPos    = 0
        , _svTime       = seconds
        }
  Context.setScene sv _gcSpriteRender
  Context.setScene sv _gcSdfRender

  let
    getTexture key = case Map.lookup key _gsTextureIds of
      Nothing ->
        error "assert: all static textures are indexed"
      Just ix ->
        Texture
          { _tBase      = 1.0
          , _tOffset    = 0
          , _tScale     = 1
          , _tSamplerId = 0
          , _tImageId   = fromIntegral ix
          }

    fitImage container size key f = (transform, f $ getTexture key)
      where
        transform =
          fit container size \(V4 cw ch cx cy) ->
            Mat4.scale cw ch 1 <> Mat4.translate cx cy 0

    fillImage container@(V4 cw ch _cx _cy) =
      fitImage container (V2 cw ch)

    putText :: V4 Float -> (Origin, Origin) -> Float -> Vec4 -> [Char] -> [(Mat4, Texture)]
    putText (V4 cw ch cx cy) (halign, valign) targetSize colorIsh =
      extract . foldl' f (V2 0 0, [])
      where
        sizeScale = targetSize / fontSize

        extract (V2 offX _offY, bits) = do -- traceShow (cw, offX * sizeScale) do
          (transform, texture) <- bits
          let
            ax = case halign of
              Begin  -> -cw / 2
              Middle -> - offX * sizeScale / 2
              End    -> cw / 2 - offX * sizeScale

            ay = case valign of
              Begin  -> ch / 2 - targetSize
              Middle -> -targetSize / 2 -- TODO: multiline centering
              End    -> -ch / 2

          pure
            ( transform
                <> Mat4.scale sizeScale sizeScale 1
                <> Mat4.translate (cx + ax) (cy + ay) 0
            , texture
            )

        f (V2 offX offY, acc) ' ' =
          ( V2 (offX + fontSize / 2) offY
          , acc
          )

        f (V2 offX offY, acc) char =
          case FontB.lookup font char of
            Nothing ->
              f (V2 offX offY, acc) '_'
            Just FontB.Sprite{size=V2 sizeX sizeY, origin=V2 originX originY, ..} ->
              ( V2 (offX + advance) offY
              , let
                  transform = mconcat
                    [ Mat4.scale
                        sizeX
                        sizeY
                        1
                    , Mat4.translate
                        (offX + sizeX / 2 - originX)
                        (offY - sizeY / 2 + originY)
                        0
                    -- , Mat4.translate
                    --     (cx - cw /2)
                    --     (cy - ch / 2)
                    --     0
                    ]

                  texture = Texture
                    { _tOffset = texOffset
                    , _tScale  = texScale
                    , _tBase   = colorIsh
                    , ..
                    }
                in
                  (transform, texture) : acc
              )

        -- Texture{..} = getTexture Textures.WHITE8_PNG
        (font@FontB.Cached{fontSize}, Texture{..}) =
          if targetSize <= 32 then
            (_gsFontCopy, getTexture Textures.UBUNTU_24_PNG)
          else
            (_gsFontHeader, getTexture Textures.ROBOTO_MEDIUM_64_PNG)

    putLines container items =
      split2Vat container (Left $ vertPadding/2) \_padTop padded ->
        concat $ cutN padded split2Vat lineHeight do
          item <- items
          pure \c ->
            putText c (Begin, Middle) 24 black item
      where
        lineHeight = 24 * 1.5
        vertPadding = ch - lineHeight * fromIntegral (length items)
          where
            V4 _cw ch _cx _cy = container

    template35i slide headerText subText points images =
      split2Hat slide (Right $ 3/5) \text pics -> concat
        [ split2Vat text (Left $ 64 * 1.5) \header body ->
            split2Vat body (Left $ 24 * 1.5) \subheader items -> concat
              [ putText header (Begin, Middle) 64 black headerText
              , case subText of
                  Left quote ->
                    putText subheader (Begin, Middle) 24 grey quote
                  Right plain ->
                    putText subheader (Begin, Middle) 24 black plain
              , putLines items points
              ]
        , splitN pics split2Vat do
            (size, key) <- images
            pure \c ->
              fitImage c size key id
        ]

    slideImages = concat [bg, contents, overlay]
      where
        fullScreen = screen extent
        (slideHeader, slideBody) = split2Vat fullScreen (Left 32) (,)

        (curSlide, flipSlide) =
          if _worldOverlayTimer >= OVERLAY_TIME/2 then
            _worldSlidePrev
          else
            (_worldSlideCur, _worldSlideFlip)

        bg = case curSlide of
          Cover ->
            [ fitImage fullScreen (V2 16 9) Textures.FPROG_SPB_1440_PNG $
                tBase .~ V4 1 1 1 0.01
            ]

          -- Final | flipSlide ->
          --   []

          _ ->
            [ fillImage fullScreen Textures.WHITE8_PNG id
            ]

        overlay =
          [ fillImage slideBody Textures.WHITE8_PNG $
              tBase .~ 1 SDL.^* (sin (pi * _worldOverlayTimer / OVERLAY_TIME))
          | _worldOverlayTimer > 0
          ]

        contents =
          pad slideHeader 32 \header ->
          pad slideBody 32 \body ->
            concat
            [ putText header (Begin, Middle) 12 black (show curSlide)
            , putText header (End, Middle) 12 black "IC Rainbow — Игровые движки как фабрики абстрактных абстракций"
            , case curSlide of
                Cover ->
                  coverSlide body flipSlide
                Jam cur ->
                  jamSlides body flipSlide cur
                Foom cur ->
                  foomSlides body flipSlide cur
                Spacemar cur ->
                  spacemarSlides body flipSlide cur
                Playground2 cur ->
                  playground2Slides body flipSlide cur
                Exstare cur ->
                  exstareSlides body flipSlide cur
                Playground3 cur ->
                  playground3Slides body flipSlide cur
                Final ->
                  finalSlide body flipSlide
            ]

    coverSlide body flipSlide = concat $
      if flipSlide then
        []
      else
        split2Vat body (Right $ 3/4) \_drop headers ->
          split2V headers \top bottom ->
              [ putText top
                  (Middle, End)
                  64 black
                  "Игровые движки"
              , pad bottom 8 \c ->
                  putText c
                    (Middle, Begin)
                    24 black
                    "как фабрики абстрактных абстракций"
              ]

    jamSlides slide flipSlide = \case
      Haskarium | flipSlide ->
        split2H slide \left right ->
        [ fitImage left (V2 800 600) Textures.TERRARIUM_JPG id
        , fitImage right (V2 1267 974) Textures.HASKARIUM_PNG id
        ]

      Haskarium ->
        template35i slide "Haskarium"
          (Right "Учебный проект для коллег")
          [ "— По мотивам “.Net Terrarium”"
          , "— Gloss"
          , "— Постепенное добавление рюшечек"
          , "— Многопользовательская расширяемость"
          , "— Последние версии всё больше походили на ECS"
          ]
          [ (V2 800 600, Textures.TERRARIUM_JPG)
          , (V2 1267 974, Textures.HASKARIUM_PNG)
          ]

      HaskellGameJam1 ->
        template35i slide "Haskell Game Jam"
          (Right "")
          [ "«Хочу как Haskarium, только публичный»"
          ]
          []

      HaskellGameJam2 | flipSlide ->
        [ fitImage slide (V2 1280 960) Textures.HGJ1_JPG id
        ]

      HaskellGameJam2 ->
        template35i slide "Haskell Game Jam 1"
          (Right "Хакатон игровых движков")
          [ "Посмотрели что есть готового для игр"
          , ""
          , "— Gloss, obvs."
          , "— SDL2: image, mixer, ttf, ~gfx~"
          , "— Apecs: +gloss, +physics"
          , "— Brick"
          ]
          [ (512, Textures.HGJ_JPG)
          ]

    foomSlides slide flipSlide = \case
      MissileCommand ->
        template35i slide "FOOM"
          (Left "«Хочу свой Missile Command!»")
          [ "— Векторная графика в оригинале"
          , "— Понятная игровая механика"
          , "— **FUN**"
          ]
          [ (V2 1536 1024, Textures.MISSILE_COMMAND_JPG)
          ]

      Foom1 ->
        template35i slide "FOOM"
          (Left "«I have no idea what I'm doing.»")
          [ "— Что-то запустилось и показывается"
          , "— Допишу ещё фишечку…"
          ]
          [ (V2 1280 720, Textures.FOOM_0_JPG)
          ]

      Foom2 | flipSlide ->
        [ fitImage slide (V2 1920 1080) Textures.FOOM_1_PNG id
        ]

      Foom2 ->
        template35i slide "FOOM"
          (Right "Что-то запустилось и показывается")
          [ "— Допишу ещё фишечку"
          , "— А теперь ещё одну"
          , "— И ещё"
          , "— СЮЖЕТ!"
          , "— Больше игровых механик!"
          ]
          [ (V2 1920 1080, Textures.FOOM_1_PNG)
          , (V2 1920 1080, Textures.FOOM_2_JPG)
          ]

      ApecsStarter1 ->
        template35i slide "Apecs+Gloss"
        (Right "Опробованные на FOOM куски кода")
        [ "— git clone https://gitlab.com/dpwiz/apecs-gloss-starter new-game"
        , "— cd new-game"
        , "— stack run"
        , "— *hack-hack-hack*"
        , "— stack run"
        , "— *hack-hack-hack*"
        , "— …"
        ]
        []

      ApecsStarter2 ->
        template35i slide "Apecs+Gloss"
        (Right "https://gitlab.com/dpwiz/apecs-gloss-starter")
        [ "— *Скучный* бойлерплейт скучен и вынесен"
        , "    • Инициализация мира"
        , "    • Запуск окошек"
        , "— Не библиотека и не фреймворк"
        , "    • Авторы вольны всё двигать как *им* будет удобней"
        , "— Коллекция обкатанных решений"
        , "    • Простые компоненты: экран, скорость, курсор"
        , "    • Позже: игровые сцены, сложные базовые компоненты"
        ]
        []

      FoomFindings | flipSlide ->
        [ fitImage slide (V2 1920 1080) Textures.FOOM_3_PNG id
        ]

      FoomItch ->
        template35i slide "FOOM -> itch.io"
        (Left "Publish or ~perish~ whatever, it's your game.")
        [ "Linux:"
        , "— hsinstall foom"
        , "— butler push Foom.appImage icrbow/foom:linux"
        , ""
        , "Windows:"
        , "— git pull --ff-only"
        , "— stack build"
        , "— butler push foom.exe icrbow/foom:windows"
        , ""
        , "MacOS:"
        , "— :shrug:"
        ]
        [ (V2 827 339, Textures.FOOM_DOWNLOAD_PNG)
        , (V2 892 411, Textures.FOOM_CHART_PNG)
        ]

      FoomFindings ->
        template35i slide "FOOM: выводы"
        (Right "*FUN* driven development")
        [ "— Код почти всегда в играбельном состоянии"
        , "— Быстрый результат"
        , "— Обозримый объём ближайших работ"
        , "— Можно допиливать ∞бесконечно∞"
        ]
        [ (V2 1920 1080, Textures.FOOM_3_PNG)
        ]

    spacemarSlides slide flipSlide = \case
      SpacemarCover ->
        putText slide (Middle, Middle) 64 black
          "Геймджемы двигатель прогресса"

      GamesMadeQuick ->
        template35i slide "Games made quick????"
        (Right "https://itch.io/jam/games-made-quick-four")
        [ "— Judging:"
        , "   None of it! Everyone who does something wins!"
        , "— Prizes:"
        , "   You will receive 1 (one) thing you made that you"
        , "   would not have otherwise made!"
        , ""
        , "Q: Can I—"
        , "A: Yes!"
        , ""
        , "Q: But I don't know how to make video games!"
        , "A: This is an excellent time to learn!"
        ]
        [ (V2 392 300, Textures.GAMES_MADE_QUICK_PNG)
        ]
      Spacewar1 ->
        template35i slide "Spacewar!"
        (Right "")
        [ "Декабрь 2019: «Хочу свой Spacewar!»"
        , ""
        , ""
        , "“Spacewar! is a space combat video game"
        , " developed in 1962 … the first known video game"
        , " to be played at multiple computer installations.”"
        , ""
        , ""
        , "Тема воркшопа: «Сделай свою *первую игру* на хаскеле!» :smile:"
        ]
        [ (V2 1280 861, Textures.SPACEWAR_1_JPG)
        ]

      Spacewar2 ->
        template35i slide "Spacewar!"
        (Right "")
        [ "— Векторная графика в оригинале"
        , "— Понятная механика"
        , "— **FUN**"
        , ""
        , "(на самом деле, конечно, боёвка из Star Control)"
        ]
        [ (V2 1280 861, Textures.SPACEWAR_2_JPG)
        ]

      SpacemarDog | flipSlide ->
        [ fitImage slide (V2 1920 1080) Textures.SPACEMAR_1_PNG id
        ]

      SpacemarDog ->
        template35i slide "SpaceMar!"
        (Left "«I still don't have any idea what I'm doing»")
        [ "— git clone https://gitlab.com/dpwiz/apecs-gloss-starter"
        , ""
        , "— «Динамика хорошая, но стрелялку делать не хочу.»"
        , ""
        ]
        [ (V2 683 623, Textures.SPACEMAR_0_PNG)
        ]

      SpacemarJam | flipSlide ->
        [ fitImage slide (V2 1920 1080) Textures.SPACEMAR_2_PNG id
        ]

      SpacemarJam ->
        template35i slide "SpaceMar!"
        (Right "4 дня геймджема")
        [ "— Играбельно с первой же версии"
        , "    • 1 центральная игровая механика"
        , "    • Уникальная фишка"
        , "    • Начало, середина и конец"
        , "    • Боты"
        , "— Всё ещё Apecs+Gloss"
        ]
        [ (V2 485 333, Textures.SPACEMAR_3_PNG)
        , (V2 488 208, Textures.SPACEMAR_4_PNG)
        ]

      SpacemarResults | flipSlide ->
        [ fitImage slide (V2 1116 175) Textures.SPACEMAR_7_PNG id
        ]

      SpacemarResults ->
        template35i slide "SpaceMar!"
        (Right "3 месяца взахлёб, 177 коммитов")
        [ "— Куча скриншотов и видео"
        , "— Регулярные сессии с повышенной нагрузкой"
        , "    • Практика профайлера и алгоритмов"
        , "— Ведение *продукта*"
        , "    • Релизы в итче на две платформы"
        , "    • Регулярные девблоги"
        , "    • Плейлист на ютубе"
        , "    • Канал в телеге"
        , "— Всегда есть что ещё улучшить “за вечер”"
        ]
        [ (V2 488 275, Textures.SPACEMAR_5_PNG)
        , (V2 488 307, Textures.SPACEMAR_6_PNG)
        ]

      SpacemArt | flipSlide ->
        [ fitImage slide (V2 1280 720) Textures.SPACEMART_2_PNG id
        ]

      SpacemArt ->
        template35i slide "SpaceMar!"
        (Right "Ограничения требуют искусства")
        [ "— Векторная графика - ассеты из ниоткуда"
        , "— Раз готова рисовалка, можно и порисовать"
        ]
        [ (V2 488 275, Textures.SPACEMART_1_PNG)
        ]

      ObservationLounge | flipSlide ->
        [ fitImage slide (V2 1280 720) Textures.LOUNGE_3_PNG id
        ]

      ObservationLounge ->
        template35i slide "SpaceMar!"
        (Right "Observation Lounge")
        [ "— Крэш курс орбитальной механики"
        , "— Отладочная площадка ботов"
        , "— Самоиграйка"
        , "— Гравитационное поле экспериментов"
        ]
        [ (V2 488 350, Textures.LOUNGE_1_PNG)
        , (V2 488 350, Textures.LOUNGE_2_PNG)
        ]

      FieldExperiments | flipSlide ->
        [ fitImage slide (V2 1280 720) Textures.FIELD_2_PNG id
        ]

      FieldExperiments ->
        template35i slide "SpaceMar!"
        (Right "Гравитационное поле экспериментов")
        [ "— Быстрый способ прогреть комнату"
        , "— Наглядная демонстрация особенностей физики"
        , "— Пределы Gloss где-то тут"
        ]
        [ (V2 1920 1080, Textures.FIELD_1_PNG)
        ]

      SpacemarFindings | flipSlide ->
        [ fitImage slide (V2 913 408) Textures.SPACEMAR_CHART_PNG id
        ]

      SpacemarFindings ->
        template35i slide "SpaceMar! - выводы"
        (Right "")
        [ "— Immediate mode не приговор"
        , "    • Естественные ограничения на scope"
        , "    • Можно тащить за счёт остальных вещей помимо графония"
        , "— Даже близко не закончено"
        , "    • Всегда завершено"
        , "    • Каждая фишка открывает несколько других"
        ]
        []

    playground2Slides slide flipSlide = \case
      Playground2Cover ->
        putText slide (Middle, Middle) 64 black
          "To give a game 46"

      LudumDare46 ->
        template35i slide "Ludum Dare 46"
        (Right "Create a game in 72 hours")
        [ "— You’re free to use any tools or libraries to create your game."
        , "— You’re free to start with any base-code you may have."
        , ""
        , "— Apecs+… Gloss?"
        , "    • Есть пара экспериментов со спрайтами и текстом"
        , "    • Регулярные заглядывания под капот gloss-rendering"
        , ""
        , "— https://github.com/benl23x5/gloss/tree/master/gloss-sdl"
        , "    • «This project is under development, don't expect anything to work yet»"
        , ""
        , "— Зря я чтоль на распродаже взял Humble 7000 Game Dev icons Bundle"
        ]
        []

      PreJam1 | flipSlide ->
        [ fitImage slide (V2 1602 939) Textures.PLAYGROUND2_2_PNG id
        ]

      PreJam1 ->
        template35i slide "Playground 2"
        (Right "Pre-jam")
        [ "~apecs-sdl2-starter~ https://gitlab.com/dpwiz/playground2"
        , ""
        , "window <- SDL.createWindow"
        , "    \"Their SDL Application\""
        , "    SDL.defaultWindow"
        , ""
        , "— Разворот парадигмы на ½τ"
        , "    • Указатели на буфера"
        , "    • Текстуры и разные сорта координат"
        , "    • Шейдеры"
        , "    • Отрисовка в IO"
        , ""
        , "— Туториалы на C и C++"
        ]
        [ (V2 1002 1039, Textures.PLAYGROUND2_0_PNG)
        , (V2 1026 807, Textures.PLAYGROUND2_1_PNG)
        ]

      PreJam2 ->
        template35i slide "Playground 2"
        (Left "«Ideas? Nope. Doing? Yes!")
        [ "initResources :: IO (GL.Program, GL.AttribLocation)"
        , "initResources = do"
        , "  vert <- compileShader GL.VertexShader vertSource"
        , "  frag <- compileShader GL.FragmentShader fragSource"
        , ""
        , "  program <- GL.createProgram"
        , "  GL.attachShader program vert"
        , "  GL.attachShader program frag"
        , "  GL.attribLocation program \"coord2d\" $= attr"
        ]
        [ (V2 1602 939, Textures.PLAYGROUND2_3_PNG)
        , (V2 1602 939, Textures.PLAYGROUND2_4_PNG)
        ]

    exstareSlides slide flipSlide = \case
      ExstareCover ->
        template35i slide "Ludum Dare 46"
        (Right "")
        [ "— Keep it alive"
        ]
        [ (V2 264 174, Textures.LD46_STARTS_PNG)
        , (V2 1602 939, Textures.EXSTARE_0_PNG)
        ]

      Exstare1 | flipSlide ->
        [ fitImage slide (V2 1602 939) Textures.EXSTARE_3_PNG id
        ]

      Exstare1 ->
        template35i slide "Exstare"
        (Left "«А вот моя новая игра, такая же как две предыдущих.»")
        [ "— Применить всё, что заботано за пару недель"
        , "    • Абстракции над шейдерами"
        , "    • Генераторы геометрии"
        , "    • Эффекты из спрайтов и GLSL-палок"
        ]
        [ (V2 1602 939, Textures.EXSTARE_1_PNG)
        , (V2 1602 939, Textures.EXSTARE_2_PNG)
        ]

      Exstare2 | flipSlide ->
        [ fitImage slide (V2 1602 939) Textures.EXSTARE_6_PNG id
        ]

      Exstare2 ->
        template35i slide "Exstare"
        (Left "«А вот моя новая игра, такая же как две предыдущих.»")
        [ "— Применить всё, что заботано за полгода"
        , "    • Паттерны над апексом"
        , "    • Профилирование"
        , "    • Ленивый код"
        , "    • Строгие данные"
        ]
        [ (V2 488 286, Textures.EXSTARE_4_PNG)
        , (V2 488 286, Textures.EXSTARE_5_PNG)
        ]

      Exstare3 | flipSlide ->
        [ fitImage slide (V2 895 417) Textures.EXSTARE_CHART_PNG id
        ]

      Exstare3 ->
        template35i slide "Exstare"
        (Left "«А вот моя новая игра, такая же как две предыдущих.»")
        [ "[ Product management intensifies ]"
        , ""
        , "— Зоопарк железа"
        , "— Жёсткий дедлайн"
        , "    • Нельзя переделывать во время голосования"
        , "    • Можно фиксить без изменения геймплея"
        , "    • Можно продолжать эксперименты в нерелизных бранчах"
        , "— Хочется доделать"
        , ""
        , "— https://ldjam.com/events/ludum-dare/46/exstare"
        , "— https://icrbow.itch.io/exstare"
        ]
        [ (V2 405 720, Textures.EXSTARE_7_PNG)
        ]

      ExstareFindings ->
        template35i slide "Exstare: выводы"
        (Right "")
        [ "— Сишные API не приговор"
        , "— «Хаскель — лучший императивый язык»"
        , "    • С указателями и ручным управлением памятью"
        , "    • С пакетами для графики"
        , "    • Без готовых движков"
        , "— Оптимизация движка - отдельный вид фана"
        , "    • Не пытайтесь без замеров"
        ]
        [ (V2 818 1097, Textures.EXSTARE_BENCH_PNG)
        ]

    playground3Slides slide flipSlide = \case
      Playground3Cover ->
        putText slide (Middle, Middle) 64 black
          "Industry forged"

      VulkanHello | flipSlide ->
        [ fitImage slide (V2 1899 1064) Textures.VULKAN_2D_PNG id
        ]

      VulkanHello ->
        template35i slide "GL, next"
        (Right "Вулканский приветмир")
        [ "— https://github.com/expipiplus1/vulkan"
        , "    • «Бойлерплейта конечно километры,"
        , "        но оно запускается.»"
        , "— hsinstall sdl-triangle"
        , "    • «Шипится нормально.»"
        , "— «Да и не так много того бойлерплейта.»"
        ]
        [ (V2 1922 1119, Textures.VULKAN_TRIANGLE_PNG)
        , (V2 1359 978, Textures.VULKAN_HEX_PNG)
        ]

      VulkanTutorial ->
        template35i slide "Vulkan"
        (Left "«Совершенно новый API»")
        [ "— Туториалы для тех, кто уже знает GL"
        , "— В целом, игроделам пофиг"
        , "— Спеки опубликованы в виде XML"
        , "— Валидация отдельная и более въедливая"
        ]
        [ (V2 372 80, Textures.VULKAN_TUTORIAL_PNG)
        , (V2 2560 1440, Textures.VULKAN_Q3_PNG)
        ]

      Playground3Then | flipSlide ->
        [ fitImage slide (V2 910 813) Textures.PLAYGROUND3_2_PNG id
        ]

      Playground3Then ->
        template35i slide "Playground 3"
        (Left "«Читаю C++, пишу хаскель со словарём»")
        [ "— Stateless API - это очень хорошо"
        , "— Регионы и владение через ResourceT"
        , "— Глитч-арт и фризы"
        ]
        [ (V2 1047 1034, Textures.PLAYGROUND3_0_PNG)
        , (V2 726 544, Textures.PLAYGROUND3_1_PNG)
        ]

      Playground3Now | flipSlide ->
        [ fitImage slide (V2 1280 720) Textures.PLAYGROUND3_5_PNG id
        ]

      Playground3Now ->
        template35i slide "Playground 3"
        (Left "«Считаю матрицы со словарём»")
        [ "— Коллекция словарей значительно расширилась"
        , "    • Шейдерные лейауты, атрибуты и буфера"
        , "    • Многопроходный рендер"
        , "    • Текстуры и свет"
        , "    • Текст"
        ]
        [ (V2 2560 1440, Textures.PLAYGROUND3_3_PNG)
        , (V2 720 524, Textures.PLAYGROUND3_4_PNG)
        ]

      Playground3Findings ->
        template35i slide "Playground 3"
        (Right "Предварительные выводы")
        [ "— Бойлерплейт не приговор"
        , "    • А повод навернуть абстракций"
        , "— Make it work. Make it right. Make it fast."
        , "— Make it *FUN*"
        ]
        [ (V2 1280 720, Textures.ABSTRACT_BOILERPLATE_1_PNG)
        , (V2 1280 720, Textures.ABSTRACT_BOILERPLATE_2_PNG)
        ]

    finalSlide slide _flipSlide =
      template35i slide "Haskell Game Dev"
        (Right "")
        [ "— Безудержная социализация"
        , "    • https://t.me/HaskellGameDev"
        , "    • https://www.reddit.com/r/haskellgamedev/"
        , "— Безудержная самоизоляция"
        , "    • https://t.me/HaskellSpaceProgram"
        , "    • https://gitlab.com/dpwiz"
        , "    • https://icrbow.itch.io/"
        ]
        [ (V2 616 616, Textures.FPROG_SPB_PNG)
        , (V2 512 512, Textures.HGJ_JPG)
        ]

    groups =
      [ slideImages
      ]

  pure $ DrawCommands transient \cmd -> do
    Vk.cmdBindPipeline cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcPipeline _gcSprite)
    Vk.cmdBindDescriptorSets cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcLayout _gcSprite) 0 (_rcDescSets _gcSpriteRender) mempty

    -- TODO: Set up separate sprite rendering pass and pipeline
    Vk.cmdBindVertexBuffers cmd 0 (pure $ _modelVertices _gsSprite) (pure 0)
    Vk.cmdBindIndexBuffer cmd (_modelIndices _gsSprite) 0 Vk.INDEX_TYPE_UINT32

    for_ (zip [0.5, 0.5 - 0.0001 ..] groups) \(layer, groupItems) -> do
      for_ groupItems \(vert, frag) -> do
        Sprite.pushVertex cmd (_pcLayout _gcSprite) (Mat4.translate 0 0 layer <> vert)
        Sprite.pushFragment cmd (_pcLayout _gcSprite) $ Sprite.FragmentPush frag

        let
          instanceCount = 1
          firstIndex    = 0
          vertexOffset  = 0
          firstInstance = 0
        Vk.cmdDrawIndexed cmd (_modelIndexCount _gsSprite) instanceCount firstIndex vertexOffset firstInstance

-------------------------------------------------------------------

data Origin
  = Begin
  | Middle
  | End
  deriving (Eq, Ord, Show, Enum, Bounded)

type Container = V4 Float

screen :: Vk.Extent2D -> Container
screen Vk.Extent2D{width, height} =
  V4 (fromIntegral width) (fromIntegral height) 0 0

pad :: Container -> Float -> (Container -> r) -> r
pad (V4 w h x y) v f = f $ V4 (w - 2*v) (h - 2*v) x y

fit :: Container -> V2 Float -> (Container -> r) -> r
fit (V4 cw ch cx cy) (V2 ow oh) f = f $ V4 (ow * scale) (oh * scale) cx cy
  where
    scale =
      if cw / ch >= ow / oh then
        ch / oh
      else
        cw / ow

type Splitter r = Container -> Either Float Float -> (Container -> Container -> r) -> r

split2Hat :: Splitter r
split2Hat container@(V4 cw ch cx cy) leftSize f =
  case leftSize of
    Left cut ->
      f
        (V4 cut        ch (cx + cut / 2 - cw / 2) cy)
        (V4 (cw - cut) ch (cx + cut / 2)          cy)

    Right alpha ->
      split2Hat container (Left $ cw * alpha) f

split2H :: Container -> (Container -> Container -> r) -> r
split2H container = split2Hat container (Right $ 1/2)

split2Vat :: Splitter r
split2Vat container@(V4 cw ch cx cy) topSize f =
  case topSize of
    Left cut ->
      f
        (V4 cw cut        cx (cy + ch / 2 - cut / 2))
        (V4 cw (ch - cut) cx (cy - cut / 2))

    Right alpha ->
      split2Vat container (Left $ ch * alpha) f

split2V :: Container -> (Container -> Container -> r) -> r
split2V container = split2Vat container (Right $ 1/2)

splitN :: Container -> Splitter (Container, Container) -> [Container -> r] -> [r]
splitN initial splitter items = final
  where
    (_c, _n, final) = foldl' f (initial, length items, mempty) items

    f (container, n, acc) proc =
      let
        (cur, next) = splitter container (Right $ 1 / fromIntegral n) (,)
      in
        (next, n - 1, proc cur : acc)

cutN :: Container -> Splitter (Container, Container) -> Float -> [Container -> r] -> [r]
cutN initial splitter slice items = final
  where
    (_c, _n, final) = foldl' f (initial, length items, mempty) items

    f (container, n, acc) proc =
      let
        (cur, next) = splitter container (Left slice) (,)
      in
        (next, n - 1, proc cur : acc)

_white :: Vec4
_white = V4 1 1 1 1

grey :: Vec4
grey = V4 0 0 0 0.75

black :: Vec4
black = V4 0 0 0 1
