module Render.Setup.Context where

import Import

import Data.Acquire (Acquire)

import qualified Foreign
import qualified RIO.Vector as Vector
import qualified RIO.Vector.Partial as Vector ((!))
import qualified Vulkan.Core10 as Vk

import qualified Vulkan.Buffer.Uniform as Uniform
import qualified Render.Setup.Acquire as Acquire

pattern MAX_TEXTURES :: (Eq a, Num a) => a
pattern MAX_TEXTURES = 128

acquire
  :: VulkanDevice
  -> VulkanContext
  -> Vector Vk.DescriptorSetLayout
  -> (Vk.DescriptorSet -> [SomeStruct Vk.WriteDescriptorSet])
  -> Acquire RenderContext
acquire vd@VulkanDevice{..} vc layout writers = do
  scene    <- Uniform.acquire vd (Just zero)
  object   <- Uniform.acquire vd (Just mempty)

  descSets <- Acquire.descriptorSets vd vc layout

  set0 <- case descSets Vector.!? 0 of
    Nothing ->
      error "Empty descSets"
    Just s ->
      pure s

  let
    writes = Vector.fromList $
      [ sceneWriter set0 0 scene
      , objectWriter set0 3 object
      ] ++ writers set0
  Vk.updateDescriptorSets _vdLogical writes mempty

  pure RenderContext
    { _rcDescSets = descSets
    , _rcScene    = scene
    , _rcObject   = object
    }

-- | Common context setup
acquire_
  :: VulkanDevice
  -> VulkanContext
  -> Vector Vk.DescriptorSetLayout
  -> Vector Vk.ImageView
  -> Acquire RenderContext
acquire_ vd vc layout textures =
  acquire vd vc layout \set0 ->
    [ texturesWriter set0 2 textures MAX_TEXTURES
    ]

layoutBindings :: Vector Vk.Sampler -> [[Vk.DescriptorSetLayoutBinding]]
layoutBindings samplers = [ set0 ]
  where
    set0 = [ binding0, binding1, binding2, binding3 ]
      where
        binding0 = Vk.DescriptorSetLayoutBinding
          { Vk.binding           = 0
          , Vk.descriptorType    = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
          , Vk.stageFlags        = Vk.SHADER_STAGE_VERTEX_BIT .|. Vk.SHADER_STAGE_FRAGMENT_BIT
          , Vk.descriptorCount   = 1
          , Vk.immutableSamplers = mempty
          }

        binding1 = Vk.DescriptorSetLayoutBinding
          { Vk.binding           = 1
          , Vk.stageFlags        = Vk.SHADER_STAGE_FRAGMENT_BIT
          , Vk.descriptorType    = Vk.DESCRIPTOR_TYPE_SAMPLER
          , Vk.descriptorCount   = fromIntegral $ Vector.length samplers
          , Vk.immutableSamplers = samplers
          }

        binding2 = Vk.DescriptorSetLayoutBinding
          { Vk.binding           = 2
          , Vk.stageFlags        = Vk.SHADER_STAGE_FRAGMENT_BIT
          , Vk.descriptorType    = Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE
          , Vk.descriptorCount   = MAX_TEXTURES
          , Vk.immutableSamplers = mempty
          }

        binding3 = Vk.DescriptorSetLayoutBinding
          { Vk.binding           = 3
          , Vk.descriptorType    = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER -- TODO: _DYNAMIC
          , Vk.stageFlags        = Vk.SHADER_STAGE_VERTEX_BIT .|. Vk.SHADER_STAGE_FRAGMENT_BIT
          , Vk.descriptorCount   = 1
          , Vk.immutableSamplers = mempty
          }

setScene
  :: (MonadReader (App context state) m, MonadUnliftIO m)
  => SceneView -> RenderContext -> m ()
setScene sv rc = do
  vd <- view appVulkanDevice
  Uniform.update vd (_rcScene rc) sv

sceneWriter :: Vk.DescriptorSet -> Word32 -> Allocated Vk.Buffer SceneView -> SomeStruct Vk.WriteDescriptorSet
sceneWriter descSet binding ubo = SomeStruct zero
  { Vk.dstSet          = descSet
  , Vk.dstBinding      = binding
  , Vk.dstArrayElement = 0
  , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
  , Vk.descriptorCount = 1
  , Vk.bufferInfo      = Vector.singleton bi
  }
  where
    bi = Vk.DescriptorBufferInfo
      { Vk.buffer = _allocated ubo
      , Vk.offset = 0
      , Vk.range  = Vk.WHOLE_SIZE
      }

samplersWriter :: Vk.DescriptorSet -> Word32 -> Vector Vk.Sampler -> SomeStruct Vk.WriteDescriptorSet
samplersWriter descSet binding samplers = SomeStruct zero
  { Vk.dstSet          = descSet
  , Vk.dstBinding      = binding
  , Vk.dstArrayElement = 0
  , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_SAMPLER
  , Vk.descriptorCount = fromIntegral $ Vector.length samplers
  , Vk.imageInfo       = samplersInfo
  }
  where
    samplersInfo = do
      sampler <- samplers
      pure Vk.DescriptorImageInfo
        { sampler     = sampler
        , imageView   = zero
        , imageLayout = zero
        }

texturesWriter :: Vk.DescriptorSet -> Word32 -> Vector Vk.ImageView -> Int -> SomeStruct Vk.WriteDescriptorSet
texturesWriter descSet binding textures maxTextures = SomeStruct zero
  { Vk.dstSet          = descSet
  , Vk.dstBinding      = binding
  , Vk.dstArrayElement = 0
  , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE
  , Vk.descriptorCount = fromIntegral $ Vector.length textureInfos
  , Vk.imageInfo       = textureInfos
  }
  where
    textureInfos = do
      texture <- textures <> fillers
      pure Vk.DescriptorImageInfo
        { sampler     = zero
        , imageView   = texture
        , imageLayout = Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        }

    fillers =
      Vector.replicate
        (maxTextures - Vector.length textures)
        (textures Vector.! 0)

textureWriter :: Vk.DescriptorSet -> Word32 -> Vk.Sampler -> Vk.ImageView -> SomeStruct Vk.WriteDescriptorSet
textureWriter descSet binding textureSampler textureView = SomeStruct zero
  { Vk.dstSet          = descSet
  , Vk.dstBinding      = binding
  , Vk.dstArrayElement = 0
  , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
  , Vk.descriptorCount = 1
  , Vk.imageInfo       = Vector.singleton textureImageInfo
  }
  where
    textureImageInfo = Vk.DescriptorImageInfo
      { sampler     = textureSampler
      , imageView   = textureView
      , imageLayout = Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
      }

setObject
  :: (MonadReader (App context state) m, MonadUnliftIO m)
  => Mat4 -> RenderContext -> m ()
setObject obj rc = do
  vd <- view appVulkanDevice
  Uniform.update vd (_rcObject rc) obj

objectWriter :: Vk.DescriptorSet -> Word32 -> Allocated Vk.Buffer Mat4 -> SomeStruct Vk.WriteDescriptorSet
objectWriter descSet binding ubo = SomeStruct zero
  { Vk.dstSet          = descSet
  , Vk.dstBinding      = binding
  , Vk.dstArrayElement = 0
  , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER -- TODO: _DYNAMIC
  , Vk.descriptorCount = 1
  , Vk.bufferInfo      = Vector.singleton bi
  }
  where
    bi = Vk.DescriptorBufferInfo
      { Vk.buffer = _allocated ubo
      , Vk.offset = 0
      , Vk.range  = Vk.WHOLE_SIZE
      }

{-# DEPRECATED pushVertex "Use rcObject" #-}
pushVertex :: MonadIO m => Vk.CommandBuffer -> Vk.PipelineLayout -> Mat4 -> m ()
pushVertex commandBuffer layout model =
  liftIO $ Foreign.with model \ptr ->
    Vk.cmdPushConstants
      commandBuffer
      layout
      Vk.SHADER_STAGE_VERTEX_BIT
      0
      (fromIntegral $ Foreign.sizeOf model)
      (Foreign.castPtr ptr)
  where
    -- TODO: modelInv
