module Render.Setup.Acquire
  ( pipeline
  , layouts
  , descriptorSets
  , pipelineLayout
  , shaderStages
  ) where

import Import hiding (sets)

import Data.Acquire (Acquire, mkAcquire)

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk

import qualified Render.Setup.Pipeline as Pipeline

-- * Pipeline

pipeline
  :: VulkanDevice
  -> VulkanContext

  -> Vector Vk.DescriptorSetLayout

  -> Pipeline.Config

  -> SpirV
  -> SpirV
  -> SomeStruct Vk.PipelineVertexInputStateCreateInfo
  -> [Vk.PushConstantRange]

  -> Acquire PipelineContext
pipeline vd vc descLayouts config vertex fragment vertexInput push = do
  _pcLayout <- pipelineLayout vd descLayouts push
  stages <- shaderStages vd vertex fragment
  _pcPipeline <- Pipeline.acquire vd vc config stages _pcLayout vertexInput
  pure PipelineContext{..}

-- * Layouts

layouts
  :: VulkanDevice
  -> [[Vk.DescriptorSetLayoutBinding]]
  -> Acquire (Vector Vk.DescriptorSetLayout)
layouts VulkanDevice{..} sets =
  Vector.forM (Vector.fromList sets) \bindings -> do
    let
      layoutCI = zero
        { Vk.bindings = Vector.fromList bindings
        }
    Vk.withDescriptorSetLayout _vdLogical layoutCI Nothing mkAcquire

-- * Sets

descriptorSets :: VulkanDevice -> VulkanContext -> Vector Vk.DescriptorSetLayout -> Acquire (Vector Vk.DescriptorSet)
descriptorSets VulkanDevice{..} VulkanContext{..} setLayouts = mkAcquire allocate don'tFree
  where
    allocate =
      Vk.allocateDescriptorSets _vdLogical zero
        { Vk.descriptorPool = _vcDescriptorPool
        , Vk.setLayouts     = setLayouts
        }

    -- XXX: Validation warning:
    --      It is invalid to call vkFreeDescriptorSets() with a pool created
    --      without setting VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT.
    don'tFree _descSet =
      pure ()

-- * Pipeline

pipelineLayout :: VulkanDevice -> Vector Vk.DescriptorSetLayout -> [Vk.PushConstantRange] -> Acquire Vk.PipelineLayout
pipelineLayout VulkanDevice{..} descLayouts pushRanges =
  Vk.withPipelineLayout _vdLogical layoutCI Nothing mkAcquire
  where
    layoutCI = zero
      { Vk.setLayouts         = descLayouts
      , Vk.pushConstantRanges = Vector.fromList pushRanges
      }

shaderStages :: VulkanDevice -> SpirV -> SpirV -> Acquire ShaderStages
shaderStages VulkanDevice{_vdLogical} vertex fragment = ShaderStages
  <$> Vk.withShaderModule _vdLogical vertexCode Nothing mkAcquire
  <*> Vk.withShaderModule _vdLogical fragmentCode Nothing mkAcquire
  where
    vertexCode = zero
      { Vk.code = unSpirV vertex
      }

    fragmentCode = zero
      { Vk.code = unSpirV fragment
      }
