module Render.Draw where

import Import

import Control.Monad.Trans.Resource (ReleaseKey, release)

import qualified RIO.Vector as Vector
import qualified RIO.Vector.Partial as Vector
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Extensions.VK_KHR_swapchain as Khr

-- import Render.Scene (Scene)

data Draw
  = DrawCommands [ReleaseKey] (Vk.CommandBuffer -> RIO AppGame ())
  -- | DrawScene Scene

drawFrame :: [Draw] -> RIO AppGame ()
drawFrame draws = do
  VulkanDevice{..} <- view appVulkanDevice
  VulkanContext{..} <- view (appContext . gcVulkan)

  SyncObject{..} <- nextSyncObject
  let inflight = pure _soFramesInFlight

  Vk.waitForFences _vdLogical inflight True maxBound >>= \case
    Vk.SUCCESS ->
      pure ()
    err ->
      error $ show err -- TODO: throw VulkanError

  imageIndex <- Khr.acquireNextImageKHR _vdLogical _vcSwapChain maxBound _soImageAvailable zero >>= \case
    (Vk.SUCCESS, imageIndex) ->
      pure imageIndex -- TODO: lookup perImage
    (err, _ii) ->
      error $ show err

  let
    commandBuffer = _vcCommandBuffers Vector.! fromIntegral imageIndex

    commandBufferBI = zero
      { Vk.flags = Vk.COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
      } :: Vk.CommandBufferBeginInfo '[]

    renderPassBI = zero
      { Vk.renderPass  = _vcRenderPass
      , Vk.framebuffer = _vcFramebuffers Vector.! fromIntegral imageIndex
      , Vk.renderArea  = rect
      , Vk.clearValues = Vector.fromList clear
      }
      where
        rect = Vk.Rect2D
          { Vk.offset = zero
          , Vk.extent = _vcExtent
          }
        clear =
          [ color
          , Vk.DepthStencil (Vk.ClearDepthStencilValue 1.0 0)
          , color
          ]
        color =
          Vk.Color (Vk.Float32 (0, 0, 0, 0))

  Vk.useCommandBuffer commandBuffer commandBufferBI $
    Vk.cmdUseRenderPass commandBuffer renderPassBI Vk.SUBPASS_CONTENTS_INLINE $
      for_ draws \case
        -- DrawScene scene ->
        --   drawScene commandBuffer scene
        DrawCommands _release drawCommands ->
          drawCommands commandBuffer

  Vk.resetFences _vdLogical inflight
  let
    submitInfo = Vector.singleton $ SomeStruct zero
      { Vk.waitSemaphores   = Vector.singleton _soImageAvailable
      , Vk.waitDstStageMask = Vector.singleton Vk.PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
      , Vk.commandBuffers   = Vector.singleton (Vk.commandBufferHandle commandBuffer)
      , Vk.signalSemaphores = Vector.singleton _soRenderFinished
      }

  -- error "WAIT! I haven't yet done my validation checks!"
  Vk.queueSubmit _vdGraphicsQ submitInfo _soFramesInFlight

  let
    presentInfo = zero
      { Khr.waitSemaphores = Vector.singleton _soRenderFinished
      , Khr.swapchains     = Vector.singleton _vcSwapChain
      , Khr.imageIndices   = Vector.singleton imageIndex
      }
  Khr.queuePresentKHR _vdPresentQ presentInfo >>= \case
    _result -> do
      -- TODO: rewrite for something faster
      -- XXX: _soRenderFinished should be waited by presentInfo
      Vk.queueWaitIdle _vdPresentQ

      traverse_ release do
        draws >>= \case
          -- DrawScene{} ->
          --   mempty
          DrawCommands releaseKeys _commands -> do
            releaseKeys

-- TODO: actually swap syncObjects to enable nultiple inflight frames
nextSyncObject :: RIO AppGame SyncObject
nextSyncObject =
  views (appVulkanDevice . vdSyncObjects) Vector.headM >>= \case
    Nothing ->
      error "no sync objects"
    Just head ->
      pure head
