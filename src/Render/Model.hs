module Render.Model where

import Import

import Data.Acquire (Acquire)

import qualified Resource.GlTF as GlTF
import qualified Resource.Object as Object

import qualified Vulkan.Buffer.Vertex as Vertex

acquireIndexed :: VertexAttribs mesh => VulkanDevice -> Indexed mesh -> Acquire Model
acquireIndexed vd mesh = do
  (vertices, indices, size) <- Vertex.acquireIndexed vd mesh

  pure Model
    { _modelVertices   = vertices
    , _modelIndices    = indices
    , _modelIndexCount = size
    , _modelTransform  = mempty
    }

acquireObj :: VulkanDevice -> FilePath -> Mat4 -> Acquire Model
acquireObj vd object transform = do
  mesh <- liftIO $ Object.load object
  (vertices, indices, size) <- Vertex.acquireIndexed vd mesh

  pure Model
    { _modelVertices   = vertices
    , _modelIndices    = indices
    , _modelIndexCount = size
    , _modelTransform  = transform
    }

acquireGltf :: VulkanDevice -> FilePath -> Mat4 -> Acquire Model
acquireGltf vd gltf transform = do
  mesh <- liftIO $ GlTF.load gltf
  (vertices, indices, size) <- Vertex.acquireIndexed vd mesh

  pure Model
    { _modelVertices   = vertices
    , _modelIndices    = indices
    , _modelIndexCount = size
    , _modelTransform  = transform
    }
