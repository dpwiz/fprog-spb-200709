module Render.Scene
  ( Scene
  , node, node_
  , leaf, leaf_

  , unfoldTree
  , unfoldForest

  , flatGroups
  , flatten
  ) where

import Import

import Data.Tree (Tree(..), unfoldForest, unfoldTree)

type Scene = Tree (Mat4, Maybe Texture)

-- * Construction

node :: Mat4 -> Texture -> [Scene] -> Scene
node tr mat children =
  Node (tr, Just mat) children

node_ :: Mat4 -> [Scene] -> Scene
node_ tr children =
  Node (tr, Nothing) children

leaf :: Mat4 -> Texture -> Scene
leaf tr mat =
  node tr mat []

leaf_ :: Texture -> Scene
leaf_ = leaf mempty

-- * Consumption

flatGroups :: Functor group => [group [Scene]] -> [group [(Mat4, Texture)]]
flatGroups = map (fmap $ concatMap flatten)

flatten :: Scene -> [(Mat4, Texture)]
flatten = fold . unfoldTree \Node{rootLabel=(curTrans, curMat), subForest} ->
  ( case curMat of
      Nothing ->
        []
      Just mat ->
        [(curTrans, mat)]
  , do
      Node (subTrans, subNode) subChildren <- subForest
      pure $ Node (subTrans <> curTrans, subNode) subChildren
  )
