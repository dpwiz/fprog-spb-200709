module Render.Texture where

import Import

import Data.Acquire (Acquire)
import qualified Vulkan.Core10 as Vk

import qualified Vulkan.Buffer.Image as Image

acquire :: VulkanDevice -> FilePath -> Acquire Vk.ImageView
acquire vd file = do
  (textureImage, mips) <- Image.acquireImage vd file
  Image.acquireImageView vd textureImage mips
