module Render.Simple.Vertex
  ( Vertex(..)
  , square
  ) where

import RIO

import Linear (V3(..), V2(..))

import Geometry.Types (Indexed(..), VertexAttribs(..))

data Vertex = Vertex
  { vPos      :: V3 Float
  , vColor    :: V3 Float
  , vTexCoord :: V2 Float
  , vNormal   :: V3 Float
  }
  deriving (Eq, Ord, Show, Generic)

instance Hashable Vertex

instance VertexAttribs Vertex where
  vertexAttribs vertices = do
    Vertex{..} <- vertices
    let V3 x y z    = vPos
    let V3 r g b    = vColor
    let V2 u v      = vTexCoord
    let V3 nx ny nz = vNormal
    [ x, y, z, r, g, b, u, v, nx, ny, nz ]

square :: Indexed Vertex
square = Indexed vertices indices
  where
    -- TODO: remove vertex color
    vertices =
      [ Vertex rightBottom (V3 0 1 0) (V2 1 0) faceNormal
      , Vertex rightTop    (V3 0 0 1) (V2 1 1) faceNormal
      , Vertex leftTop     (V3 1 0 0) (V2 0 1) faceNormal
      , Vertex leftBottom  (V3 1 1 1) (V2 0 0) faceNormal
      ]

    indices =
      [ 0, 1, 2
      , 2, 3, 0
      ]

    leftTop     = V3 (-0.5) (-0.5) 0
    rightTop    = V3 ( 0.5) (-0.5) 0
    rightBottom = V3 ( 0.5) ( 0.5) 0
    leftBottom  = V3 (-0.5) ( 0.5) 0

    faceNormal = V3 0 0 1
