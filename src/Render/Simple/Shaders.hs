{-# LANGUAGE QuasiQuotes #-}

module Render.Simple.Shaders
  ( vertex
  , fragment

  , vertexInput
  , vertexConstants
  , fragmentConstants
  ) where

import Import

import Vulkan.Utils.ShaderQQ (frag, vert)

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk

import Render.Setup.Pipeline (attrBindings, formatSize)

vertex :: SpirV
vertex = SpirV
  [vert|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    // layout(push_constant) uniform MAX_SIZE_128_CRY {
    //   mat4 model;
    // } unused_;

    layout(set=0, binding=0) uniform PER_FRAME {
      mat4 view;
      mat4 projection;
      vec4 viewPosTime;
    } scene;

    layout(set=0, binding=3) uniform ObjectTransform {
      mat4 model;
      // TODO: mat4 modelInv;
    } object;

    layout(location = 0) in vec3 inPosition;
    layout(location = 1) in vec3 inColor;
    layout(location = 2) in vec2 inTexCoord;
    layout(location = 3) in vec3 inNormal;

    layout(location = 4) in vec4 model_l0;
    layout(location = 5) in vec4 model_l1;
    layout(location = 6) in vec4 model_l2;
    layout(location = 7) in vec4 model_l3;

    layout(location = 0) out vec3 fragPosition;
    layout(location = 1) out vec3 fragColor;
    layout(location = 2) out vec2 fragTexCoord;
    layout(location = 3) out vec3 fragNormal;

    void main() {
      // XXX: reassemble mat4 from instance attributes
      mat4 model = mat4(model_l0, model_l1, model_l2, model_l3);

      gl_Position = scene.projection * scene.view * model * vec4(inPosition, 1.0);
      // gl_Position = model * vec4(inPosition, 1.0);
      fragColor = inColor;
      fragTexCoord = inTexCoord;
      fragNormal = transpose(mat3(inverse(model))) * inNormal; // TODO: use modelInv
      fragPosition = vec3(model * vec4(inPosition, 1.0));
    }
  |]

fragment :: SpirV
fragment = SpirV
  [frag|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    // TODO: move to materials
    float matShiny = 32;
    float matSpecular = 1.0;

    struct Texture {
      vec4 base;
      vec4 texOffsetScale;
      uint samplerId;
      uint textureId;
    };

    struct Light {
      vec4 color;     // a is intensity
      vec4 position;  // w is attenuation power 0/1/2
      vec4 direction; // a is ambient spill
      vec2 cutoff;    // inner / outer
    };

    layout(push_constant) uniform MAX_SIZE_128_CRY {
      layout(offset=64) Texture diffuse;
    } material;

    layout(set=0, binding=0) uniform PER_FRAME {
      mat4 view;
      mat4 projection;
      vec4 viewPosTime;
    } scene;

    layout(set=0, binding=1) uniform sampler samplers[8];
    layout(set=0, binding=2) uniform texture2D textures[128];

    layout(location = 0) in vec3 fragPosition;
    layout(location = 1) in vec3 fragColor;
    layout(location = 2) in vec2 fragTexCoord;
    layout(location = 3) in vec3 fragNormal;

    layout(location = 0) out vec4 outColor;

    // TODO: move lights to dynamic UBO array
    // TODO: add bitmask
    const int lightNum = 3;
    Light lights[3] = {
      { vec4(
            vec3(1.0),
            8.0
            + 0.25 * sin(scene.viewPosTime.w * 3)
            + 0.25 * cos(scene.viewPosTime.w * 5)
          )
      , vec4(vec3(0, -2, 0), 2)
      , vec4(
          normalize(vec3(cos(scene.viewPosTime.w), 5, sin(scene.viewPosTime.w))),
          0.05
        )
      , cos(radians(vec2(15, 27.5)))
      },
      { vec4(
            vec3(1.0),
            16.0
            + 0.25 * sin(scene.viewPosTime.w * 3)
            + 0.25 * cos(scene.viewPosTime.w * 5)
          )
      , vec4(vec3(0, 1, 0), 0.25)
      , vec4(
          normalize(vec3(cos(scene.viewPosTime.w), 5, sin(scene.viewPosTime.w))),
          0
        )
      , cos(radians(vec2(35, 67.5)))
      },
      { vec4(
            vec3(-1),
            16.0
            + 0.25 * sin(scene.viewPosTime.w * 3)
            + 0.25 * cos(scene.viewPosTime.w * 5)
          )
      , vec4(vec3(0, 1, 0), 0.25)
      , vec4(
          normalize(vec3(cos(scene.viewPosTime.w), 5, sin(scene.viewPosTime.w))),
          0
        )
      , cos(radians(vec2(33, 67)))
      }
    };

    float when_gt(float x, float y) {
      return max(sign(x - y), 0.0);
    }

    void main() {
      // XXX: can't do per-vertex due to interpolation artifacts
      vec3 normal = normalize(fragNormal);

      vec2 uv = material.diffuse.texOffsetScale.xy + material.diffuse.texOffsetScale.zw * fragTexCoord;
      vec4 diffTex = texture(
        sampler2D(
          textures[material.diffuse.textureId],
          samplers[material.diffuse.samplerId]
        ),
        uv
      );

      vec3 light = vec3(0);
      for (int i=0; i < lightNum; i++) {
        vec3 beam = lights[i].position.xyz - fragPosition;
        float attenuation = pow(1 / dot(beam, beam), lights[i].position.w);
        vec3 lightDir = normalize(beam);
        vec3 energy = lights[i].color.rgb * lights[i].color.a * attenuation;

        // Flat ambient spill
        light += energy * lights[i].direction.a;

        // Spot light
        float theta = dot(-lightDir, normalize(lights[i].direction.xyz));
        float epsilon = lights[i].cutoff.x - lights[i].cutoff.y;
        float spot = smoothstep(
          0.0, 1.0,
          (theta - lights[i].cutoff.y) / epsilon
        );
        energy *= spot;

        // Diffuse lighting

        float diff = max(dot(normal, lightDir), 0.0);

        light += energy * diff;

        // Specular highlights
        vec3 viewDir = normalize(scene.viewPosTime.xyz - fragPosition);
        float spec;

        // // Phong specs:
        // vec3 reflectDir = reflect(-lightDir, normal);
        // spec = pow(max(dot(viewDir, reflectDir), 0.0), matShiny) * attenuation;

        // Blinn specs:
        vec3 halfwayDir = normalize(lightDir + viewDir);
        spec = pow(max(dot(normal, halfwayDir), 0.0), matShiny);

        light += energy
          * when_gt(diff, 0.0f)
          * matSpecular
          * spec;
      }

      outColor = diffTex * vec4(light, 1.0);
    }
  |]

vertexInput :: SomeStruct Vk.PipelineVertexInputStateCreateInfo
vertexInput = SomeStruct zero
  { Vk.vertexBindingDescriptions = Vector.fromList
      [ Vk.VertexInputBindingDescription
          { binding   = 0
          , stride    = sum $ map formatSize vertexAttrs
          , inputRate = Vk.VERTEX_INPUT_RATE_VERTEX
          }
      , Vk.VertexInputBindingDescription
          { binding   = 1
          , stride    = sum $ map formatSize instanceAttrs
          , inputRate = Vk.VERTEX_INPUT_RATE_INSTANCE
          }
      ]
  , Vk.vertexAttributeDescriptions =
      attrBindings [vertexAttrs, instanceAttrs]
  }

vertexAttrs :: [Vk.Format]
vertexAttrs =
  [ Vk.FORMAT_R32G32B32_SFLOAT
  , Vk.FORMAT_R32G32B32_SFLOAT
  , Vk.FORMAT_R32G32_SFLOAT
  , Vk.FORMAT_R32G32B32_SFLOAT
  ]

instanceAttrs :: [Vk.Format]
instanceAttrs =
  [ Vk.FORMAT_R32G32B32A32_SFLOAT
  , Vk.FORMAT_R32G32B32A32_SFLOAT
  , Vk.FORMAT_R32G32B32A32_SFLOAT
  , Vk.FORMAT_R32G32B32A32_SFLOAT
  ]

vertexConstants :: Vk.PushConstantRange
vertexConstants = Vk.PushConstantRange
  { Vk.stageFlags = Vk.SHADER_STAGE_VERTEX_BIT
  , Vk.size       = fromIntegral $ sizeOf @Mat4 mempty
  , Vk.offset     = 0
  }

fragmentConstants :: Vk.PushConstantRange
fragmentConstants = Vk.PushConstantRange
  { Vk.stageFlags = Vk.SHADER_STAGE_FRAGMENT_BIT
  , Vk.offset     = offset
  , Vk.size       = fromIntegral $ sum pushFields
  }
  where
    Vk.PushConstantRange{size=offset} = vertexConstants

    pushFields =
      [ sizeOf @Texture zero -- diffuse
      ]
