module Render.Simple.Context
  ( acquire
  , Context.pushVertex
  , Context.setScene
  , Context.setObject

  , pushFragment
  , FragmentPush(..)
  ) where

import Import

import Data.Acquire (Acquire)

import qualified Foreign
import qualified Vulkan.Core10 as Vk

import qualified Render.Setup.Acquire as Acquire
import qualified Render.Setup.Context as Context
import qualified Render.Setup.Pipeline as Pipeline
import qualified Render.Simple.Shaders as Shaders

acquire
  :: VulkanDevice
  -> VulkanContext
  -> Vector Vk.ImageView
  -> Acquire (PipelineContext, RenderContext)
acquire vd vc textures = do
  layouts <- Acquire.layouts vd (Context.layoutBindings $ _vdSamplers vd)

  pc <- Acquire.pipeline
    vd vc
    layouts
    (Pipeline.Config True True False Vk.CULL_MODE_BACK_BIT)
    Shaders.vertex Shaders.fragment Shaders.vertexInput [Shaders.vertexConstants, Shaders.fragmentConstants]

  rc <- Context.acquire_ vd vc layouts textures

  pure (pc, rc)

pushFragment :: MonadIO m => Vk.CommandBuffer -> Vk.PipelineLayout -> FragmentPush -> m ()
pushFragment commandBuffer layout value =
  liftIO $ Foreign.with value \ptr ->
    Vk.cmdPushConstants
      commandBuffer
      layout
      Vk.SHADER_STAGE_FRAGMENT_BIT
      off
      (fromIntegral $ sizeOf value)
      (Foreign.castPtr ptr)
  where
    Vk.PushConstantRange{size=off} = Shaders.vertexConstants

newtype FragmentPush = FragmentPush Texture
  deriving (Eq, Storable, Zero)
