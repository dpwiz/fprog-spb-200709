module Render.Solid.Vertex
  ( Vertex(..)
  , square
  , square2
  , rectXY
  ) where

import RIO

import Linear (V4(..), V3(..))

import Geometry.Types (Indexed(..), VertexAttribs(..))

-- | Vertex attributes for Render.Solid.Shaders
data Vertex = Vertex
  { solidColor    :: V4 Float
  , solidPosition :: V3 Float
  }
  deriving (Eq, Ord, Show, Generic)

instance Hashable Vertex

instance VertexAttribs Vertex where
  vertexAttribs vertices = do
    Vertex{..} <- vertices
    let V4 r g b a = solidColor
    let V3 x y z   = solidPosition
    [r, g, b, a, x, y, z]

square :: Indexed Vertex
square = Indexed vertices indices
  where
    vertices =
      [ Vertex (V4 0 1 0 1) rightBottom
      , Vertex (V4 0 0 1 1) rightTop
      , Vertex (V4 1 0 0 1) leftTop
      , Vertex (V4 1 1 1 1) leftBottom
      ]

    indices =
      [ 0, 1, 2
      , 2, 3, 0
      ]

    leftTop     = V3 (-0.5) (-0.5) 0
    rightTop    = V3 ( 0.5) (-0.5) 0
    rightBottom = V3 ( 0.5) ( 0.5) 0
    leftBottom  = V3 (-0.5) ( 0.5) 0

-- | Double-sided square
square2 :: Indexed Vertex
square2 = Indexed
  { iItems   = iItems
  , iIndices = iIndices <> reverse iIndices
  }
  where
    Indexed{..} = square

-- | XY-plane rectangle from two triangles.
rectXY :: V4 Float -> Float -> Float -> (V3 Float -> V3 Float) -> Indexed Vertex
rectXY color w h tr = Indexed vertices (indices <> reverse indices)
  where
    vertices =
      [ Vertex color (tr rightBottom)
      , Vertex color (tr rightTop)
      , Vertex color (tr leftTop)
      , Vertex color (tr leftBottom)
      ]

    indices =
      [ 0, 1, 2
      , 2, 3, 0
      ]

    leftTop     = V3 (-0.5 * w) (-0.5 * h) 0
    rightTop    = V3 ( 0.5 * w) (-0.5 * h) 0
    rightBottom = V3 ( 0.5 * w) ( 0.5 * h) 0
    leftBottom  = V3 (-0.5 * w) ( 0.5 * h) 0
