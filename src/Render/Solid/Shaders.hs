{-# LANGUAGE QuasiQuotes #-}

module Render.Solid.Shaders where

import Import

import Vulkan.Utils.ShaderQQ (frag, vert)

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk

import Render.Setup.Pipeline (attrBindings, formatSize)

vertex :: SpirV
vertex = SpirV
  [vert|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(push_constant) uniform MAX_SIZE_128_CRY {
      mat4 model;
      // TODO: modelInv;
    } obj;

    layout(set=0, binding=0) uniform PER_FRAME {
      mat4 view;
      mat4 projection;
      vec4 viewPosTime;
    } scene;

    layout(location = 0) in vec4 inColor;
    layout(location = 1) in vec3 inPosition;

    layout(location = 0) out vec4 fragColor;

    void main() {
      gl_Position = scene.projection * scene.view * obj.model * vec4(inPosition.xyz, 1.0);
      fragColor = inColor;
    }
  |]

fragment :: SpirV
fragment = SpirV
  [frag|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(location = 0) in vec4 fragColor;

    layout(location = 0) out vec4 outColor;

    void main() {
      outColor = fragColor;
    }
  |]

vertexInput :: SomeStruct Vk.PipelineVertexInputStateCreateInfo
vertexInput = SomeStruct zero
  { Vk.vertexBindingDescriptions = Vector.singleton zero
      { Vk.binding = 0
      , Vk.stride  = sum $ map formatSize vertexAttrs
      }
  , Vk.vertexAttributeDescriptions =
      attrBindings [vertexAttrs]
  }

vertexAttrs :: [Vk.Format]
vertexAttrs =
  [ Vk.FORMAT_R32G32B32A32_SFLOAT
  , Vk.FORMAT_R32G32B32_SFLOAT
  ]

vertexConstants :: Vk.PushConstantRange
vertexConstants = Vk.PushConstantRange
  { Vk.stageFlags = Vk.SHADER_STAGE_VERTEX_BIT
  , Vk.size       = fromIntegral $ sizeOf @Mat4 mempty
  , Vk.offset     = 0
  }
