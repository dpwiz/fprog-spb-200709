{-# LANGUAGE QuasiQuotes #-}

module Render.Sprite.Shaders
  ( vertex
  , fragment

  , vertexInput
  , vertexConstants
  , fragmentConstants
  ) where

import Import

import Vulkan.Utils.ShaderQQ (frag, vert)

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk

import Render.Setup.Pipeline (attrBindings, formatSize)

vertex :: SpirV
vertex = SpirV
  [vert|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(push_constant) uniform MAX_SIZE_128_CRY {
      layout(offset=0) mat4 model;
      // TODO: modelInv;
    } obj;

    layout(set=0, binding=0) uniform PER_FRAME {
      mat4 view;
      mat4 projection;
      vec4 viewPosTime;
    } scene;

    // TODO: instance uniforms

    layout(location = 0) in vec2 inPosition;
    layout(location = 1) in vec2 inTexCoord;

    layout(location = 0) out vec2 fragTexCoord;

    void main() {
      gl_Position = scene.projection * scene.view * obj.model * vec4(inPosition, 0.0, 1.0);
      fragTexCoord = inTexCoord;
    }
  |]

fragment :: SpirV
fragment = SpirV
  [frag|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    struct Texture {
      vec4 base;
      vec4 texOffsetScale;
      uint samplerId;
      uint textureId;
    };

    layout(push_constant) uniform MAX_SIZE_128_CRY {
      layout(offset=64) Texture primary;
    } obj;

    layout(set=0, binding=1) uniform sampler samplers[8];
    layout(set=0, binding=2) uniform texture2D textures[128];

    layout(location = 0) in vec2 fragTexCoord;

    layout(location = 0) out vec4 outColor;

    void main() {
      vec2 uv = obj.primary.texOffsetScale.xy + obj.primary.texOffsetScale.zw * fragTexCoord;

      vec4 texel = texture(
        sampler2D(
          textures[obj.primary.textureId],
          samplers[obj.primary.samplerId]
        ),
        uv
      );

      outColor = texel * obj.primary.base;
    }
  |]

vertexInput :: SomeStruct Vk.PipelineVertexInputStateCreateInfo
vertexInput = SomeStruct zero
  { Vk.vertexBindingDescriptions = Vector.singleton zero
      { Vk.binding = 0
      , Vk.stride  = sum $ map formatSize vertexAttrs
      }
  , Vk.vertexAttributeDescriptions =
      attrBindings [vertexAttrs]
  }

vertexAttrs :: [Vk.Format]
vertexAttrs =
  [ Vk.FORMAT_R32G32_SFLOAT
  , Vk.FORMAT_R32G32_SFLOAT
  ]

vertexConstants :: Vk.PushConstantRange
vertexConstants = Vk.PushConstantRange
  { Vk.stageFlags = Vk.SHADER_STAGE_VERTEX_BIT
  , Vk.offset     = 0
  , Vk.size       = fromIntegral $ sizeOf @Mat4 mempty
  }

fragmentConstants :: Vk.PushConstantRange
fragmentConstants = Vk.PushConstantRange
  { Vk.stageFlags = Vk.SHADER_STAGE_FRAGMENT_BIT
  , Vk.offset     = offset
  , Vk.size       = fromIntegral $ sum pushFields
  }
  where
    Vk.PushConstantRange{size=offset} = vertexConstants

    pushFields =
      [ sizeOf @Texture zero
      ]
