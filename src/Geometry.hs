module Geometry where

import Import

import qualified RIO.HashMap as HashMap

indexed :: (Eq a, Hashable a, Foldable t) => t a -> Indexed a
indexed xs = Indexed
  { iItems   = reverse unique -- XXX: indices are counted from the right-most element.
  , iIndices = indices        -- XXX: using left fold would only @add@ a 'reverse' here.
  }
  where
    (_seen, unique, indices) = foldr go (mempty, mempty, mempty) xs

    go item (seen, itemAcc, indexAcc) =
      case HashMap.lookup item seen of
        Just oldIx ->
          (seen, itemAcc, oldIx : indexAcc)
        Nothing ->
          let
            newIx = fromIntegral $ HashMap.size seen
          in
            ( HashMap.insert item newIx seen
            , item : itemAcc
            , newIx : indexAcc
            )
