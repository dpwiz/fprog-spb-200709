-- | Temporary adapters for Geomancy

module Geometry.Projection where

import Import

import qualified Vulkan.Core10 as Vk

import Geomancy (vec3)
import Geomancy.Projection (lookAtRH, perspectiveRH, orthoOffCenterRH)

-- TODO: add quaternion rotation to Geomancy for Vec3 arguments
looking
  :: "origin" ::: V3 Float
  -> "target" ::: V3 Float
  -> Mat4
looking origin' target' = lookAtRH origin target up
  where
    origin =
      let
        V3 x y z  = origin'
      in
        vec3 x y z

    target =
      let
        V3 x y z  = target'
      in
        vec3 x y z

    up =
      vec3 0 (-1) 0

perspective
  :: Vk.Extent2D
  -> "fov degs" ::: Float
  -> "near"     ::: Float
  -> "far"      ::: Float
  -> Mat4
perspective Vk.Extent2D{width, height} fovDegs near far =
  perspectiveRH fovDegs near far width height

orthographic :: Vk.Extent2D -> Mat4
orthographic Vk.Extent2D{width, height} =
  orthoOffCenterRH 0.001 (-1) width height
