{-# LANGUAGE TemplateHaskell #-}

module Geometry.Types where

import RIO

import Control.Lens.TH (makeLenses)
import Geomancy.Mat4 (Mat4)
import Linear (V2, V3, V4)
import Vulkan.Zero (Zero(..))

import qualified Foreign

class VertexAttribs a where
  vertexAttribs :: [a] -> [Float]

data Indexed a = Indexed
  { iItems   :: [a]
  , iIndices :: [Word32]
  }
  deriving (Show, Functor, Foldable, Traversable)

instance Semigroup (Indexed a) where
  -- XXX: half-assed, but should work
  a <> b = Indexed
    { iItems   = iItems a <> iItems b
    , iIndices = iIndices a <> map ((+) ixOffset) (iIndices b)
    }
    where
      ixOffset = fromIntegral $ length (iItems a)

instance Monoid (Indexed a) where
  mempty = Indexed mempty mempty

data SceneView = SceneView
  { _svView       :: Mat4
  , _svProjection :: Mat4
  , _svViewPos    :: V3 Float
  , _svTime       :: Float
  } deriving (Show)

instance Zero SceneView where
  zero = SceneView
    { _svView       = mempty
    , _svProjection = mempty
    , _svViewPos    = 0
    , _svTime       = 0
    }

instance Storable SceneView where
  sizeOf ~SceneView{..} = sum
    [ Foreign.sizeOf _svView
    , Foreign.sizeOf _svProjection
    , Foreign.sizeOf _svViewPos
    , Foreign.sizeOf _svTime
    ]

  alignment = const 16

  poke ptr SceneView{..} = do
    Foreign.poke        (Foreign.castPtr ptr)            _svView
    Foreign.pokeByteOff (Foreign.castPtr ptr) (64)       _svProjection
    Foreign.pokeByteOff (Foreign.castPtr ptr) (64+64)    _svViewPos
    Foreign.pokeByteOff (Foreign.castPtr ptr) (64+64+12) _svTime

  peek ptr = SceneView
    <$> Foreign.peek        (Foreign.castPtr ptr)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) 64
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (64+64)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (64+64+12)

data Texture = Texture
  { _tBase      :: V4 Float
  , _tOffset    :: V2 Float
  , _tScale     :: V2 Float
  , _tSamplerId :: Word32
  , _tImageId   :: Word32
  } deriving (Eq, Show)

instance Zero Texture where
  zero = Texture
    { _tBase      = 0
    , _tOffset    = 0
    , _tScale     = 0
    , _tSamplerId = 0
    , _tImageId   = 0
    }

instance Storable Texture where
  sizeOf ~Texture{..} = sum
    [ Foreign.sizeOf _tBase      -- 16
    , Foreign.sizeOf _tOffset    -- 8
    , Foreign.sizeOf _tScale     -- 8
    , Foreign.sizeOf _tSamplerId -- 4
    , Foreign.sizeOf _tImageId   -- 4
    ]

  alignment = const 4

  poke ptr Texture{..} = do
    Foreign.poke        (Foreign.castPtr ptr)            _tBase
    Foreign.pokeByteOff (Foreign.castPtr ptr) (16)       _tOffset
    Foreign.pokeByteOff (Foreign.castPtr ptr) (16+8)     _tScale
    Foreign.pokeByteOff (Foreign.castPtr ptr) (16+8+8)   _tSamplerId
    Foreign.pokeByteOff (Foreign.castPtr ptr) (16+8+8+4) _tImageId

  peek ptr = Texture
    <$> Foreign.peek        (Foreign.castPtr ptr)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) 16
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (16+8)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (16+8+8)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (16+8+8+4)

makeLenses ''SceneView
makeLenses ''Texture
