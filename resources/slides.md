---
author: 'IC Rainbow'
title: 'Игровые движки как фабрика абстрактных абстракций'
patat:
  pandocExtensions:
    - patat_extensions
    - autolink_bare_uris
    - emoji
  _images:
    backend: w3m
    path: /usr/lib/w3m/w3mimgdisplay
...

# Writing boilerplate makes you a better programmer

## Writing boilerplate makes you a better programmer

![cube](resources/fprog-spb.jpg)

## Haskarium

Учебный проект для коллег.

. . .

![](resources/haskarium.jpg)

- Вдохновлено «.Net Terrarium»
- Gloss
- Постепенное добавление рюшечек

## Haskell Game Jam

> Хочу как хаскариум, только публичный.

## Haskell Game Jam

- Хакатон движков
- Посмотрели что есть готового для игр
    * Gloss
    * SDL2: image, mixer, ttf, ~~gfx~~
    * Brick
    * Apecs: +gloss, +physics

# FOOM

## FOOM

> Хочу свой Missile Command!

. . .

![](resources/missile-command.jpg)

- Векторная графика в оригинале
- Понятная механика
- **FUN**

## FOOM

![](resources/foom-0.jpg)

https://gitlab.com/dpwiz/foom

## FOOM

> I have no idea what I'm doing
> — Some dog on the internet.

. . .

- Что-то запустилось и показывается
- Допишу ещё фишечку
- А теперь ещё одну
- И ещё
- СЮЖЕТ!
- Больше игровых механик!

## Apecs + Gloss

Опробованные на FOOM куски кода.

. . .

- git clone https://gitlab.com/dpwiz/apecs-gloss-starter new-game
- cd new-game
- stack run
- *hack-hack-hack*
- stack run
- *hack-hack-hack*
- ...

## Apecs + Gloss

https://gitlab.com/dpwiz/apecs-gloss-starter

- *Скучный* бойлерплейт скучен и вынесен
    * Инициализация мира
    * Запуск окошек
- Не библиотека и не фреймворк
    * Авторы вольны всё двигать как *им* будет удобней
- Коллекция обкатанных решений
    * Простые компоненты: экран, скорость, курсор
    * Позже: игровые сцены, сложные базовые компоненты

## FOOM - выводы

![](resources/foom-1.jpg)

- *FUN* driven development
- Код почти всегда в играбельном состоянии
- Быстрый результат
- Обозримый scope
- Можно допиливать бесконечно

## FOOM - выводы

- На Gloss можно делать игры
    * Immediate mode не приговор

- На Haskell можно делать игры
    * GC не приговор
    * `stack run --profile`
    * `{-# LANGUAGE StrictData #-}`

- Игры действительно хорошо прокачивают
    * Разнообразие задач
    * Практические и переносимые навыки
    * *FUN*

## FOOM -> itch.io

https://icrbow.itch.io/foom

> Publish or ~~perish~~ whatever, it's your game.

## FOOM -> itch.io

Linux:

* hsinstall foom
* butler push Foom.appImage icrbow/foom:linux

https://github.com/dino-/hsinstall

## FOOM -> itch.io

Windows:

* git pull --ff-only
* stack build
* butler push foom.exe icrbow/foom:windows

https://github.com/haskell-game/sdl2

## FOOM -> itch.io

MacOS:

* :shrug:

# Геймджемы двигатель прогресса

## Games made quick????

https://itch.io/jam/games-made-quick-four

. . .

- Judging: None of it!  Everyone who does something wins!
- Prizes: You will receive 1 (one) thing you made that you would not have otherwise made!

## Spacewar!

> Хочу свой Spacewar!

. . .

> Spacewar! is a space combat video game developed in 1962

> ... the first known video game to be played at multiple computer installations.

. . .

> Тема хакатона: «Сделай свою *первую игру* на хаскеле!» :smile:

## Spacewar!

![](resources/spacewar.jpg)

- Векторная графика в оригинале
- Понятная механика
- **FUN**

## SpaceMar!

> Динамика хорошая, а стреляться не хочу. Plot twist!

* git clone https://gitlab.com/dpwiz/apecs-gloss-starter spacemar

## SpaceMar!

![](resources/spacemar-0.jpg)

. . .

> I still don't have any idea what I'm doing :dog:

## SpaceMar!

![](resources/spacemar-1.jpg)

- 4 дня геймджема

- Играбельно с первой же версии
    * Центральная игровая механика
    * Уникальная фишка
    * Начало, середина и конец

- Всё ещё Apecs+Gloss

## SpaceMar!

![](resources/spacemar-2.jpg)

- 3 месяца взахлёб, 177 коммитов

- Ведение *продукта*
    * Релизы на две платформы
    * Регулярные девблоги
    * Плейлист на ютубе
    * Канал в телеге

## SpaceMar! Observation Lounge

![](resources/spacemar-3.jpg)

- Крэш курс орбитальной механики
- Отладочная площадка ботов
- Самоиграйка
- Гравитационное поле экспериментов

## SpaceMar! гравитационное поле экспериментов

![](resources/spacemar-4.jpg)

- Арт-объекты
- Бенчмарк
- Расширение apecs-gloss-starter
- Технические демки и стенды
    * apecs-random
    * gloss-sdl2-surface + sdl2-ttf

## SpaceMar! - выводы

![](resources/spacemar-5.jpg)

- Immediate mode не приговор
    * Творчество требует ограничений

- Даже близко не закончено
    * Всегда завершено
    * Каждая фишка открывает несколько других

# To give a game 46

## Ludum Dare 46

https://ldjam.com/events/ludum-dare/46/

- Игра с нуля
- Неделя на разработку
- Рейтинги и соревнования

. . .

> Купил иконок на распродаже, надо заиспользовать как спрайты.

https://gitlab.com/dpwiz/gloss-sdl2-surface

. . .

> Как-то сложненько.

## Pre-jam

Apecs... без Gloss?

. . .

https://hackage.haskell.org/package/gloss-rendering

. . .

https://github.com/benl23x5/gloss/tree/master/gloss-sdl

> This project is under development, don't expect anything to work yet

:pensive:

## Pre-jam

~~apecs-sdl2-starter~~ https://gitlab.com/dpwiz/playground2

. . .

![](resources/playground2-0.jpg)

```haskell
  window <- SDL.createWindow "Their SDL Application" SDL.defaultWindow
```

## Playground2

![](resources/playground2-1.jpg)

```haskell
initResources :: IO (GL.Program, GL.AttribLocation)
initResources = do
  vert <- compileShader GL.VertexShader vertSource
  frag <- compileShader GL.FragmentShader fragSource

  program <- GL.createProgram
  GL.attachShader program vert
  GL.attachShader program frag
  GL.attribLocation program "coord2d" $= attr

  -- ...
```

## Playground2

![](resources/playground2-2.jpg)

> Ideas? Nope. :dog:

## Playground2

![](resources/playground2-3.jpg)

> Doing? Yes! :dog:

## Ludum Dare 46

> Keep it alive.

. . .

![](resources/exstare-0.jpg)


## Exstare

> А вот моя новая игра, такая же как две предыдущих.

![](resources/exstare-1.jpg)

https://gitlab.com/dpwiz/exstare

## Exstare

- Две недели чтобы заботать SDL и OpenGL
    * Сишные и крестовые туториалы
    * GLSL
    * Проблемы с совместимостью

- [ Product management intensifies ]

- https://ldjam.com/events/ludum-dare/46/exstare

- https://icrbow.itch.io/exstare

## Exstare - выводы

- Сишные API не приговор

. . .

- Хаскель - лучший императивый язык ™
    * С указателями и ручным управлением памятью
    * С пакетами для графики
    * Без готовых движков

. . .

> OpenGL сложненький. А что там за ~~GLnext~~ Vulkan?

# Industry forged

## Playground3

https://github.com/expipiplus1/vulkan

![](resources/playground3-0.jpg)

> Бойлерплейта конечно километры, но оно запускается. :thinking:

. . .

`hsinstall sdl-triangle`

> Шипится нормально. Да и не так много того бойлерплейта... :innocent:

## Playground3

![](resources/playground3-1.jpg)

## Playground3 (три месяца спустя)

![](resources/playground3-2.jpg)

- unsafePerformUnliftedSIMDMatrixMultiplyMonoid 🤪
    * https://gitlab.com/dpwiz/geomancy
    * https://gitlab.com/dpwiz/gltf
    * ...

## Vulkan-tutorial

Ожидание:

> Возьму-ка я этот новый API, тогда не придётся оглядываться на 25 лет истории OpenGL.

Реальность:

> Это туториал по вулкану, тут некоторые вещи устроены иначе. Поэтому чтобы вам было проще
> мы перенастроим всё так, чтобы было почти как в OpenGL.

. . .

> :facepalm:

## Playground3 - предварительные выводы

![](resources/playground3-3.jpg)

- Бойлерплейт не приговор
    * А повод навернуть абстракций
- Make it work. Make it right. Make it fast.

https://gitlab.com/dpwiz/playground3

## Vulkan - предварительные выводы

![](resources/playground3-4.jpg)

- Читаю C++ со словарём
- Считаю матрицы со словарём
- И вообще коллекция словарей сильно пополнилась
    * Шейдерные лейауты, атрибуты и буфера
    * Текстуры
    * Свет
    * Текст

## Eyes on the prize

- Очередь чтения пополнилась на двадцать учебников по графике
- Постепенно приближаюсь обратно к Exstare
- *FUN*

## Haskell Game Dev

- Безудержная социализация
    * https://t.me/HaskellGameDev
    * https://www.reddit.com/r/haskellgamedev/

- Безудержная самоизоляция
    * https://t.me/HaskellSpaceProgram
    * https://gitlab.com/dpwiz
    * https://icrbow.itch.io/
