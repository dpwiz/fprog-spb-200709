let
    ghcVer = "ghc882";

    syspkgs = import <nixpkgs> {};
    pinpkgs = syspkgs.fetchFromGitHub {
       owner = "NixOS";
       repo = "nixpkgs";
       rev = "024a6dc45b6bcefa5b1886a27de01f38f2eb19e5";
       sha256 = "08dyfrig5l52isn14k451gy6mxn50r9r8na9l9qqr1z1gzp9bdv0";
    };
    pkgs = import pinpkgs {};
    allhies = import (fetchTarball "https://github.com/infinisil/all-hies/tarball/master") {};
    hie = allhies.versions.${ghcVer};
in
    pkgs.mkShell rec {
        LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath (buildInputs);
        buildInputs = [ hie hie.compiler ] ++ (with pkgs; [
            pkgconfig stack
            libffi libGLU freeglut
        ]);
    }
